package toc.mobilestudy.misc.drawpad;

/**
 * Created by user on 5/30/2016.
 * If this file contains no copyrighted material,
 * then feel free to call it yours.
 */

import android.Manifest;
import android.app.Activity;
import android.app.WallpaperManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ComposePathEffect;
import android.graphics.CornerPathEffect;
import android.graphics.DiscretePathEffect;
import android.graphics.EmbossMaskFilter;
import android.graphics.LinearGradient;
import android.graphics.MaskFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathEffect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.SweepGradient;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import toc.mobilestudy.Constants;
import yuku.ambilwarna.AmbilWarnaDialog;
import toc.mobilestudy.R;

public class MainActivity extends AppCompatActivity {
    private static final int PERMISSION = 3564;
    private int mPenWidth;
    private String BG = "background_color";

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION:
                for (int i = 0; i < permissions.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_DENIED)
                        Toast.makeText(this, "Permission denied: " + permissions[i] + ". This may cause " +
                                "the app to behave abnormally", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_draw_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        updateStatus();
        float[] outerR = new float[]{12, 12, 12, 12, 0, 0, 0, 0};
        RectF inset = new RectF(6, 6, 6, 6);
        float[] innerR = new float[]{12, 12, 0, 0, 12, 12, 0, 0};

        mDrawables = new ShapeDrawable[2];

        mDrawables[0] = new ShapeDrawable(new RoundRectShape(outerR, inset, null));
        mDrawables[0].getPaint().setShader(makeSweep());

        mDrawables[1] = new ShapeDrawable(new RoundRectShape(outerR, inset, innerR));
        mDrawables[1].getPaint().setShader(makeLinear());

        PathEffect mPathEffect1 = new DiscretePathEffect(10, 4);
        PathEffect mPathEffect2 = new CornerPathEffect(4);

        mDrawables[0].getPaint().setPathEffect(new ComposePathEffect(mPathEffect2, mPathEffect1));
        LinearLayout layout = (LinearLayout) findViewById(R.id.myDrawing);

        activity = this;
        mView = new DrawingView(this);
        layout.addView(mView, new LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));

        init();

        mEmboss = new EmbossMaskFilter(new float[]{1, 1, 1}, 0.4f, 6, 3.5f);
        mBlur = new BlurMaskFilter(8, BlurMaskFilter.Blur.NORMAL);
    }

    private void updateStatus() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDefaultDisplayHomeAsUpEnabled(true);
            Window window = getWindow();
            if (Build.VERSION.SDK_INT >= 21) {
                actionBar.setElevation(0);
            }

            actionBar.setDisplayOptions(ActionBar.DISPLAY_HOME_AS_UP | ActionBar.DISPLAY_SHOW_TITLE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.setStatusBarColor(getResources().getColor(R.color.primary));
            }
            actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimaryDark)));

        }
    }


    private static Shader makeLinear() {
        return new LinearGradient(0, 0, 50, 50, new int[]{0xFFFF0000, 0xFF00FF00, 0xFF0000FF}, null, Shader.TileMode.MIRROR);
    }

    private static Shader makeSweep() {
        return new SweepGradient(150, 25, new int[]{0xFFFF0000, 0xFF00FF00, 0xFF0000FF, 0xFFFF0000}, null);
    }

    private ShapeDrawable[] mDrawables;
    private MaskFilter mEmboss;
    private MaskFilter mBlur;
    private Paint mPaint;
    Activity activity;
    View mView;

    private void init() {
        mPaint = new Paint();
        mPaint.setDither(true);
        mPaint.setColor(Color.BLACK);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPenWidth = 3;
        mPaint.setStrokeWidth(mPenWidth);
    }

    class DrawingView extends View {
        private Path path;
        private Bitmap mBitmap;
        private Canvas mCanvas;
        int width, height;

        public DrawingView(Context context) {
            super(context);
            path = new Path();
            mBitmap = Bitmap.createBitmap(820, 480, Bitmap.Config.ARGB_8888);
            mCanvas = new Canvas(mBitmap);
            this.setBackgroundColor(Color.WHITE);
        }

        private ArrayList<PathWithPaint> _graphics1 = new ArrayList<>();

        public boolean onTouchEvent(MotionEvent event) {
            PathWithPaint pp = new PathWithPaint();
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                path.moveTo(event.getX(), event.getY());
                path.lineTo(event.getX(), event.getY());
            } else if (event.getAction() == MotionEvent.ACTION_MOVE) {
                path.lineTo(event.getX(), event.getY());
            } else if (event.getAction() == MotionEvent.ACTION_UP) {
                path.lineTo(event.getX(), event.getY());
                mCanvas.drawPath(path, mPaint);
                pp.setPath(path);
                pp.setmPaint(mPaint);
                _graphics1.add(pp);
            }
            invalidate();
            return true;
        }

        @Override
        public void onDraw(Canvas canvas) {

            canvas.drawBitmap(mBitmap, 0, 0, mPaint);

            if (_graphics1.size() > 0) {
                canvas.drawPath(_graphics1.get(_graphics1.size() - 1).getPath(), _graphics1.get(_graphics1.size() - 1).getmPaint());
            }

            Display display = getWindowManager().getDefaultDisplay();
            int width = 300;
            int height = 425;
            try {
                Method mGetRawH = Display.class.getMethod("getRawHeight");
                Method mGetRawW = Display.class.getMethod("getRawWidth");
                width = (Integer) mGetRawW.invoke(display);
                height = (Integer) mGetRawH.invoke(display);
            } catch (SecurityException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }

            int x = 0;
            int y = 0;
            if (curveDrawable == 1) {
                mDrawables[0].setBounds(x, y, x + width, (y + height) - 35);
                mDrawables[0].draw(canvas);
            }
            if (curveDrawable == 2) {
                mDrawables[1].setBounds(x, y, x + width, (y + height) - 35);
                mDrawables[1].draw(canvas);
            }
        }

        public void clearView() {
            _graphics1.removeAll(_graphics1);
            mBitmap = Bitmap.createBitmap(width, height,
                    Bitmap.Config.ARGB_8888);
            mCanvas = new Canvas(mBitmap);
            path = new Path();
            invalidate();
        }

        @Override
        public void invalidate() {
            super.invalidate();
        }

        @Override
        protected void onSizeChanged(int w, int h, int oldw, int oldh) {
            super.onSizeChanged(w, h, oldw, oldh);
            width = w;
            height = h;
            mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
            mCanvas = new Canvas(mBitmap);
        }

        public void colorChange() {
            path = new Path();
            invalidate();
        }

        public void saveAsJpg(File f, View v, boolean setWallpaper) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                int hasWallpaperPermission = checkSelfPermission(Manifest.permission.SET_WALLPAPER);
                int hasStoragePermission = checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE);
                List<String> permissions = new ArrayList<>();
                if (setWallpaper)
                    if (hasWallpaperPermission != PackageManager.PERMISSION_GRANTED)
                        permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
                if (hasStoragePermission != PackageManager.PERMISSION_GRANTED)
                    permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
                if (!permissions.isEmpty())
                    requestPermissions(permissions.toArray(new String[permissions.size()]), PERMISSION);
            }
            String fname = f.getAbsolutePath();
            FileOutputStream fos;
            try {
                fos = new FileOutputStream(fname);
                Bitmap b = Bitmap.createBitmap(v.getWidth(), v.getHeight(),
                        Bitmap.Config.ARGB_8888);
                Canvas c = new Canvas(b);
                v.draw(c);

                if (setWallpaper) {
                    WallpaperManager myWallpaperManager = WallpaperManager
                            .getInstance(getApplicationContext());
                    try {
                        myWallpaperManager.setBitmap(b);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Toast.makeText(activity, "Wallpaper has been set",
                            Toast.LENGTH_LONG).show();
                } else {
                    b.compress(CompressFormat.JPEG, 100, fos);
                    Toast.makeText(activity, "Image Saved", Toast.LENGTH_LONG)
                            .show();
                }
            } catch (Exception ex) {
                Toast.makeText(activity, "Error Saving Image",
                        Toast.LENGTH_LONG).show();
                Log.i("Drawpad", "stacktrace is " + ex);
            }
        }
    }

    private boolean isDrawBorder = true;
    int bgcolor = Color.WHITE;

    public void colorChanged(String key, int color) {

        if (key.equals("")) {
            mPaint = new Paint();
            mPaint.setDither(true);
            mPaint.setColor(color);
            mPaint.setStyle(Paint.Style.STROKE);
            mPaint.setStrokeJoin(Paint.Join.ROUND);
            mPaint.setStrokeCap(Paint.Cap.ROUND);
            mPaint.setStrokeWidth(mPenWidth);
        } else if (key.equals(BG)) {
            if (isDrawBorder)
                ((DrawingView) mView).invalidate();
            else
                ((DrawingView) mView).clearView();
            ((DrawingView) mView).setBackgroundColor(color);
            bgcolor = color;
            isDrawBorder = false;
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_draw, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private int curveDrawable = 0;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        File myDir = new File(Constants.DEFAULT_DIR,
                "Drawings");
        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_REMOVED)) {
            if (!myDir.mkdirs() && !myDir.exists())
                Snackbar.make(findViewById(R.id.myDrawing), "Directory 'Drawings' could not be created",
                        Snackbar.LENGTH_LONG).setAction(null, null).show();
        } else
            Snackbar.make(findViewById(R.id.myDrawing),
                    "No sdcard or sdcard is unreadable, save option won't work as expected", Snackbar.LENGTH_LONG).show();
        Date now = new Date();
        String fname = "image" + now.getDate() + now.getSeconds() + ".jpg";
        File file = new File(myDir, fname);

        switch (item.getItemId()) {

            case R.id.menu_color:
                ((DrawingView) mView).colorChange();
                new AmbilWarnaDialog(this, Color.BLACK, new AmbilWarnaDialog.OnAmbilWarnaListener() {
                    @Override
                    public void onCancel(AmbilWarnaDialog dialog) {

                    }

                    @Override
                    public void onOk(AmbilWarnaDialog dialog, int color) {
                        colorChanged("", color);
                    }
                }).show();
                return true;

            case R.id.menu_bg_color:
                ((DrawingView) mView).colorChange();
                new AmbilWarnaDialog(this, Color.BLACK, new AmbilWarnaDialog.OnAmbilWarnaListener() {
                    @Override
                    public void onCancel(AmbilWarnaDialog dialog) {

                    }

                    @Override
                    public void onOk(AmbilWarnaDialog dialog, int color) {
                        colorChanged(BG, color);
                    }
                }).show();
                return true;

            case R.id.menu_emboss:
                if (mPaint.getMaskFilter() != mEmboss) {
                    mPaint.setMaskFilter(mEmboss);
                } else {
                    mPaint.setMaskFilter(null);
                }
                return true;

            case R.id.menu_blur:
                if (mPaint.getMaskFilter() != mBlur) {
                    mPaint.setMaskFilter(mBlur);
                } else {
                    mPaint.setMaskFilter(null);
                }
                return true;

            case R.id.menu_erase:
                ((DrawingView) mView).colorChange();
                mPaint = new Paint();
                mPaint.setDither(true);
                mPaint.setColor(bgcolor);
                mPaint.setStyle(Paint.Style.STROKE);
                mPaint.setStrokeJoin(Paint.Join.ROUND);
                mPaint.setStrokeCap(Paint.Cap.ROUND);
                mPaint.setStrokeWidth(mPenWidth + 10);
                return true;


            case R.id.menu_set_wallpaper:
                ((DrawingView) mView).saveAsJpg(file, mView, true);
                return true;

            case R.id.menu_save:
                ((DrawingView) mView).saveAsJpg(file, mView, false);
                return true;

            case R.id.menu_clear:
                ((DrawingView) mView).clearView();
                return true;

            case R.id.menu_border_curve:
                isDrawBorder = true;
                colorChanged(BG, bgcolor);
                curveDrawable = 1;
                return true;

            case R.id.menu_border_rectangele:
                isDrawBorder = true;
                colorChanged(BG, bgcolor);
                curveDrawable = 2;
                return true;

            case R.id.menu_no_border:
                isDrawBorder = true;
                colorChanged(BG, bgcolor);
                curveDrawable = 0;
                return true;
            case R.id.menu_penwidth:
                setPenWidth();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    public void setPenWidth() {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.sample_demo_view, (ViewGroup) findViewById(R.id.demo));


        final SeekBar seekBar = (SeekBar) layout.findViewById(R.id.pageSlider);
        seekBar.setMax(296);
        final TextView textView = (TextView) layout.findViewById(R.id.endTextView);
        final DemoView demoView = (DemoView) layout.findViewById(R.id.demo_view);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (b) {
                    demoView.setRadius(i / 2);
                    mPenWidth = i;
                    textView.setText(mPenWidth + " pixels");
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        new AlertDialog.Builder(MainActivity.this)
                .setTitle("Set Pen Size")
                .setView(layout)
                .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        mPaint.setDither(true);
                        mPaint.setStyle(Paint.Style.STROKE);
                        mPaint.setStrokeJoin(Paint.Join.ROUND);
                        mPaint.setStrokeCap(Paint.Cap.ROUND);
                        mPaint.setStrokeWidth(mPenWidth);

                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

}
