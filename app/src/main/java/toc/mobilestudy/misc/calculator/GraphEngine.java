package toc.mobilestudy.misc.calculator;

import toc.mobilestudy.R;

import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.view.KeyEvent;
import android.widget.Button;
import android.view.View.OnClickListener;
import android.view.View;
import android.content.Intent;
import android.content.res.Configuration;
import android.app.Dialog;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ArrayAdapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;

public class GraphEngine extends Fragment {

    public static final int UNIQUE_ID = 3;

    private GraphView graph;
    private Dialog zeros;
    private ArrayAdapter<String> zerosArr;
    private static final String help_text = "-Press and drag to move the graph.\n-Pinch to zoom.\n" +
            "-The fns button takes you to the function entry screen.\n-Reset will reset the bounds " +
            "of the graph to default values.\n-Zeros will bring up a list of all the zeros on the " +
            "screen. Press back to return from the list. Note - if the zero is any type of minima " +
            "or maxima it will not show, this will be fixed by the next update.\n-Trace switches " +
            "from regular graphing to trace mode. Press again to switch back.\n\n-Please submit " +
            "bugs and feature requests to studycafe2016@gmail.com";

    Drawable btn_default;

    View v;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.graph, container, false);
        return v;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        graph = (GraphView) v.findViewById(R.id.graph);
        AppGlobals appState = (AppGlobals) getActivity().getApplicationContext();
        appState.setGraphView(graph);

        Button n = (Button) v.findViewById(R.id.graph_fns);
        n.setOnClickListener(fnsBtn);
        n = (Button) v.findViewById(R.id.graph_reset);
        n.setOnClickListener(resetBtn);
        n = (Button) v.findViewById(R.id.graph_zeros);
        n.setOnClickListener(zerosBtn);

        zerosArr = new ArrayAdapter<String>(getActivity(), R.layout.list_item);
        zeros = new Dialog(getActivity());
        zeros.setContentView(R.layout.zeros_disp);
        zeros.setTitle("Zeros (Y Intercepts)");
        zeros.setCancelable(true);

        n = (Button) v.findViewById(R.id.graph_trace);
        n.setOnClickListener(traceBtn);
        btn_default = n.getBackground();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    private OnClickListener fnsBtn = new OnClickListener() {
        public void onClick(View v) {
            graph.graphMode = graph.GRAPH;
            Intent myIntent = new Intent(v.getContext(), GraphFunctionsEntry.class);
            startActivityForResult(myIntent, 0);
        }
    };

    private OnClickListener resetBtn = new OnClickListener() {
        public void onClick(View v) {
            graph.reset();
        }
    };

    private OnClickListener zerosBtn = new OnClickListener() {
        public void onClick(View v) {
            zerosArr.clear();
            graph.zeros(zerosArr);
            ListView lst = (ListView) zeros.findViewById(R.id.zeros_disp);
            lst.setAdapter(zerosArr);
            zeros.show();
        }
    };

    private OnClickListener traceBtn = new OnClickListener() {
        public void onClick(View v) {
            graph.trace();
            Button trace = (Button) v.findViewById(R.id.graph_trace);
            if (graph.graphMode == graph.TRACE) {
                trace.setBackgroundResource(R.color.grey);
            } else {
                trace.setBackgroundDrawable(btn_default);
            }
        }
    };

    private void showHelp(FragmentManager fm) {
        AppGlobals.showTextDialog(fm, "Graph Help", help_text);
    }


    public void handleOptionsItemSelected(FragmentManager fm, int itemId) {
        // Handle item selection
        switch (itemId) {
            case R.id.menu_reset:
                graph.reset();
                break;
            case R.id.menu_functions:
                graph.graphMode = graph.GRAPH;
                Intent myIntent = new Intent(getContext(), GraphFunctionsEntry.class);
                startActivityForResult(myIntent, 0);
                break;
            case R.id.menu_zeros:
                zerosArr.clear();
                graph.zeros(zerosArr);
                ListView lst = (ListView) zeros.findViewById(R.id.zeros_disp);
                lst.setAdapter(zerosArr);
                zeros.show();
                break;
            case R.id.menu_trace:
                graph.trace();
                break;
            case R.id.graph_help:
                help();
                break;
            case R.id.settings:
                break;
        }
    }

    public void help() {
        final TextView noteName = new TextView(getContext());
        final ScrollView container = new ScrollView(getContext());
        container.setBackgroundColor(getResources().getColor(R.color.colorPrimaryTransparent));
        noteName.setPadding(5, 5, 5, 5);
        noteName.setText(Html.fromHtml(help_text));
        noteName.setTextColor(getResources().getColor(R.color.uno_theme));
        container.addView(noteName);
        new AlertDialog.Builder(getContext())
                .setTitle("Help")
                .setMessage("Help List")
                .setView(container)
                .setOnKeyListener(new DialogInterface.OnKeyListener() {
                    @Override
                    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                        ArrayList<Integer> keys = new ArrayList<>();
                        keys.add(KeyEvent.KEYCODE_DPAD_CENTER);
                        keys.add(KeyEvent.KEYCODE_ENTER);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                            keys.add(KeyEvent.KEYCODE_ESCAPE);
                        }
                        keys.add(KeyEvent.KEYCODE_BACK);
                        if (event.getAction() == KeyEvent.ACTION_DOWN && keys.contains(keyCode))
                            dialog.dismiss();
                        return false;
                    }
                })
                .show();
    }

}