package toc.mobilestudy.studyarea;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.preference.PreferenceManager;
import android.util.Log;

import java.math.BigInteger;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import toc.mobilestudy.Constants;
import toc.mobilestudy.DateHelper;

/**
 * Created by user on 6/28/2016.
 */

public class StudyAreaConstants {
    public static final String KEY_ID = "_id";
    public static final String KEY_TYPE = "item_type";
    public static final String KEY_DATE_TAKEN = "date_taken";
    public static final String KEY_DATE_MODIFIED = "date_modified";
    public static final String KEY_NAME = "item_name";
    public static final String KEY_LINK = "item_link";
}
