package toc.mobilestudy.misc.rteditor;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.SearchManager;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.preference.PreferenceManager;
import android.provider.SearchRecentSuggestions;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import toc.mobilestudy.Constants;
import toc.mobilestudy.R;
import toc.mobilestudy.fileexplorer.FileDialog;
import toc.mobilestudy.misc.fileexplorer.FileViewArrayAdapter;
import toc.mobilestudy.misc.rteditor.library.RTEditText;
import toc.mobilestudy.misc.rteditor.library.RTManager;
import toc.mobilestudy.misc.rteditor.library.RTToolbar;
import toc.mobilestudy.misc.rteditor.library.api.RTApi;
import toc.mobilestudy.misc.rteditor.library.api.RTMediaFactoryImpl;
import toc.mobilestudy.misc.rteditor.library.api.RTProxyImpl;
import toc.mobilestudy.misc.rteditor.library.api.format.RTFormat;
import toc.mobilestudy.misc.texttray.EditorActivity;
import toc.mobilestudy.misc.texttray.FileAutoCompleteArrayAdapter;
import toc.mobilestudy.misc.texttray.SearchSuggestions;
import toc.mobilestudy.note.NoteContentProvider;
import toc.mobilestudy.note.NoteItem;

public class RTEditorActivity extends RTEditorBaseActivity implements FileDialog.FileListener {
    private final static int REQUEST_FILE_BROWSER_OPEN = 482;
    private final static int REQUEST_FILE_BROWSER_SAVE = 483;

    // dialog ids
    //TODO: make this have a toolbar
    private final static int DIALOG_SAVE_FILE = 1;
    private final static int DIALOG_OPEN_FILE = 2;
    private final static int DIALOG_SHOULD_SAVE = 3;
    private final static int DIALOG_OVERWRITE = 4;
    private final static int DIALOG_SAVE_ERROR = 5;
    private final static int DIALOG_SAVE_ERROR_PERMISSIONS = 6;
    private final static int DIALOG_SAVE_ERROR_SDCARD = 7;
    private final static int DIALOG_READ_ERROR = 8;
    private final static int DIALOG_NOTFOUND_ERROR = 9;
    private final static int DIALOG_SHOULD_SAVE_INTENT = 13;
    private final static int DIALOG_MODIFIED = 14;

    private final static int DIALOG_SAVE_FILE_AUTOCOMPLETE = 10;
    private final static int DIALOG_OPEN_FILE_AUTOCOMPLETE = 11;

    private final static int DIALOG_RECENT_FILE_DIALOG = 12;

    // file format ids
    private final static int FILEFORMAT_NL = 1;
    private final static int FILEFORMAT_CR = 2;
    private final static int FILEFORMAT_CRNL = 3;

    // other activities
    private final static int REQUEST_CODE_PREFERENCES = 1;
    protected CharSequence filename = "";
    protected long lastModified = 0;
    protected boolean untitled = true;

    static private List<String> items = null;
    static private List<String> recentItems = null;

    protected EditText saveDialog_fne = null;
    protected EditText openDialog_fne = null;

    protected AlertDialog openRecentDialog;
    protected ListView openRecentListView;
    protected static FileViewArrayAdapter recentFilesAdapter;

    private boolean backFromFileBrowser = false;
    private CharSequence fileBrowserReturnFile;

    // some state variables

    private boolean creatingFile = false;
    private boolean savingFile = false;
    private boolean openingFile = false;
    private boolean openingError = false;
    private boolean openingRecent = false;
    private boolean sendingAttachment = false;
    private static CharSequence temp_filename = "";

    private boolean fromIntent = false;
    private boolean openingIntent = false;
    private Intent newIntent = null;

    private boolean fromSearch = false;
    private String queryString = "";

    private CharSequence errorFname = "File";
    private boolean errorSaving = false;

    private int fileformat;

    private RTManager mRTManager;

    private RTEditText mRTMessageField;


    private boolean mUseDarkTheme;
    private boolean mSplitToolbar;
    private boolean autoComplete;
    private Bundle mSavedInstanceState;
    private boolean textChanged = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // read parameters
        if (savedInstanceState == null) {
            Intent intent = getIntent();
            mUseDarkTheme = intent.getBooleanExtra(PARAM_DARK_THEME, false);
            mSplitToolbar = intent.getBooleanExtra(PARAM_SPLIT_TOOLBAR, false);
        } else {
            mUseDarkTheme = savedInstanceState.getBoolean(PARAM_DARK_THEME, false);
            mSplitToolbar = savedInstanceState.getBoolean(PARAM_SPLIT_TOOLBAR, false);
        }

        // set theme
        setTheme(mUseDarkTheme ? R.style.ThemeDark : R.style.ThemeLight);

        super.onCreate(savedInstanceState);
        mSavedInstanceState = savedInstanceState;
        updateOptions();
        Intent intent = getIntent();
        newIntent(intent);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mRTManager != null) {
            mRTManager.onDestroy(true);
        }
    }

    //TODO: linewrap
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putBoolean("creatingFile", creatingFile);
        savedInstanceState.putBoolean("savingFile", savingFile);
        savedInstanceState.putBoolean("openingFile", openingFile);
        savedInstanceState.putBoolean("openingError", openingError);
        savedInstanceState.putBoolean("openingRecent", openingRecent);
        savedInstanceState.putBoolean("openingIntent", openingIntent);
        savedInstanceState.putBoolean("sendingAttachment", sendingAttachment);

        savedInstanceState.putString("temp_filename", temp_filename.toString());
        mRTManager.onSaveInstanceState(savedInstanceState);


        savedInstanceState.putBoolean(PARAM_DARK_THEME, mUseDarkTheme);
        savedInstanceState.putBoolean(PARAM_SPLIT_TOOLBAR, mSplitToolbar);
        super.onSaveInstanceState(savedInstanceState);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        creatingFile = savedInstanceState.getBoolean("creatingFile");
        savingFile = savedInstanceState.getBoolean("savingFile");
        openingFile = savedInstanceState.getBoolean("openingFile");
        openingError = savedInstanceState.getBoolean("openingError");
        openingRecent = savedInstanceState.getBoolean("openingRecent");
        openingIntent = savedInstanceState.getBoolean("openingIntent");
        sendingAttachment = savedInstanceState.getBoolean("sendingAttachment");

        temp_filename = savedInstanceState.getString("temp_filename");
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_PREFERENCES) {
            updateOptions();
        } else if (requestCode == REQUEST_FILE_BROWSER_OPEN && data != null) {

            fileBrowserReturnFile = data.getAction();
            backFromFileBrowser = true;

            createDialog(FileDialog.Mode.PICK_FILE, DIALOG_OPEN_FILE);

        } else if (requestCode == REQUEST_FILE_BROWSER_SAVE && data != null) {

            fileBrowserReturnFile = data.getAction();
            backFromFileBrowser = true;

            createDialog(FileDialog.Mode.SAVE_FILE, DIALOG_SAVE_FILE);
        }
    }

    protected void onResume() {
        super.onResume();

        if (!fromIntent)
            myResume();
        fromIntent = false;
    }

    private void myResume() {
        try {
            // make sure the correct options are being used
            updateOptions();

            // load the previous text
            SharedPreferences prefs = getPreferences(MODE_PRIVATE);
            String restoredText = prefs.getString("text", null);
            String titleText = prefs.getString("fntext", null);

            openingIntent = prefs.getBoolean("openingIntent", false);

            int selectionStart = prefs.getInt("selection-start", -1);
            int selectionEnd = prefs.getInt("selection-end", -1);

            lastModified = prefs.getLong("lastModified", lastModified);
            filename = prefs.getString("filename", null);
            if (filename == null || filename == "")
                untitled = true;
            else
                untitled = false;

            fileformat = prefs.getInt("fileformat", FILEFORMAT_NL);

            // clear double quote problem
            if (restoredText != null) {
                int nq = prefs.getInt("text-quotes", 0);
                if (nq != 0 && countQuotes(restoredText) == 2 * nq)
                    restoredText = restoredText.replaceAll("\"\"", "\"");
            }

            if (titleText != null) {
                int nq = prefs.getInt("fntext-quotes", 0);
                if (nq != 0 && countQuotes(titleText) == 2 * nq)
                    titleText = titleText.replaceAll("\"\"", "\"");
            }

            if (filename != null) {
                int nq = prefs.getInt("filename-quotes", 0);
                if (nq != 0 && countQuotes(filename.toString()) == 2 * nq)
                    filename = filename.toString().replaceAll("\"\"", "\"");
            }


            if (restoredText != null && mRTMessageField != null) {
                mRTMessageField.setRichTextEditing(true, restoredText);

                if (selectionStart != -1 && selectionEnd != -1) {
                    mRTMessageField.setSelection(selectionStart, selectionEnd);
                }
            }

            if (titleText != null) {
                titleText = titleText.toString().replace("*", "");
                setTitle("*" + titleText.toString().substring(titleText.toString().lastIndexOf("/") + 1));
            }

            if (mRTMessageField != null)
                mRTMessageField.requestFocus();

            // for some reason I have to reset the text at the end to get the caret at the beginning
            // this has to happen at the end for some reason. I think it needs a little bit of delay.
            if (mRTMessageField != null && (selectionStart == 0 && selectionEnd == 0)) {
                if (restoredText != null) {
                    mRTMessageField.setRichTextEditing(true, restoredText);
                }

                if (titleText != null) {
                    titleText = titleText.toString().replace("*", "");
                    setTitle("*" + titleText.toString().substring(titleText.toString().lastIndexOf("/") + 1));
                }
            }

            // search search search
            if (fromSearch) {
                int start;
                String t = "";
                if ((mRTMessageField != null ? mRTMessageField.getText() : null) != null)
                    t = (mRTMessageField.getText().toString().toLowerCase());

                start = t.indexOf(queryString.toLowerCase(), selectionStart + 1);
                if (start == -1)    // loop search
                    start = t.indexOf(queryString.toLowerCase(), 0);

                if (start != -1) {
                    mRTMessageField.setSelection(start, start + queryString.length());
                } else {
                    Toast.makeText(this, "\"" + queryString + "\" not found", Toast.LENGTH_SHORT).show();
                }

                fromSearch = false;
            }
        } catch (Exception e) {
            createNew();
        }

        // figure out if the the file has been previously modified
        if (!creatingFile && !savingFile && !openingFile && !openingError && !openingRecent
                && !openingIntent && !sendingAttachment) {
            if (lastModified != 0 && lastModified != (new File(filename.toString())).lastModified()) {
                showDialog(DIALOG_MODIFIED);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_rteditor, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle all of the possible menu actions.
        openingFile = false;
        creatingFile = false;

        switch (item.getItemId()) {
            case R.id.action_text_type:
                Intent plainIntent = new Intent(RTEditorActivity.this, EditorActivity.class);
                plainIntent.setAction(EditorActivity.PLAIN_TEXT);
                plainIntent.putExtra(EditorActivity.PLAIN_TEXT, mRTMessageField.getText().toString());
                startActivity(plainIntent);
                finish();
                break;
            case R.id.action_save:    // Save
                savingFile = true;
                if (untitled) {
                    createDialog(FileDialog.Mode.SAVE_FILE, DIALOG_SAVE_FILE);
                } else
                    saveNote(filename);
                break;
            case R.id.action_save_as: // Save as
                savingFile = true;

                createDialog(FileDialog.Mode.SAVE_FILE, DIALOG_SAVE_FILE);
                break;

            case R.id.action_recent: // Open Recent List
                openingRecent = true;

                if (isTextChanged())
                    showDialog(DIALOG_SHOULD_SAVE);
                else
                    showDialog(DIALOG_RECENT_FILE_DIALOG);

                break;
            case R.id.action_open: // Open
                openingFile = true;
                openingIntent = false;

                if (isTextChanged())
                    showDialog(DIALOG_SHOULD_SAVE);
                else {
                    createDialog(FileDialog.Mode.PICK_FILE, DIALOG_OPEN_FILE);
                }

                break;
            case R.id.action_settings: // Options
                startActivityForResult(new Intent(this, RTEditPreferences.class), REQUEST_CODE_PREFERENCES);

                break;
            case R.id.action_new: // Newfile
                creatingFile = true;
                if (isTextChanged())
                    showDialog(DIALOG_SHOULD_SAVE);
                else
                    createNew();

                break;

            case R.id.action_email_content: // Email Text
                Intent sendIntent = new Intent(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, mRTMessageField.getText(RTFormat.HTML));
                sendIntent.setType("message/rfc822");
                startActivity(Intent.createChooser(sendIntent, "Send email with"));
                break;

            case R.id.action_search: // Trigger search
                this.onSearchRequested();
                break;

            case R.id.action_email_attachment: // Email Attachment
                sendingAttachment = true;

                if (isTextChanged())
                    showDialog(DIALOG_SHOULD_SAVE);
                else if (untitled) {
                    Toast.makeText(this, R.string.onSendEmptyMessage, Toast.LENGTH_SHORT).show();
                } else
                    sendAttachment();

                break;

            case R.id.action_send_uno: // Trigger search
                startActivity(new Intent(RTEditorActivity.this, toc.mobilestudy.StartActivity.class));
                break;

        }

        return super.onOptionsItemSelected(item);
    }


    protected void onPause() {
        super.onPause();

        // save the preferences
        SharedPreferences.Editor editor = getPreferences(MODE_PRIVATE).edit();

        if (editor != null && mRTMessageField != null) {
            String t;

            t = mRTMessageField.getText(RTFormat.HTML);
            editor.putString("text", t);
            editor.putInt("text-quotes", countQuotes(t));

            t = getTitle().toString();
            editor.putString("fntext", t);
            editor.putInt("fntext-quotes", countQuotes(t));

            if (filename != null)
                t = filename.toString();
            else
                t = "";
            editor.putString("filename", t);
            editor.putLong("lastModified", lastModified);
            editor.putInt("filename-quotes", countQuotes(t));

            editor.putInt("selection-start", mRTMessageField.getSelectionStart());
            editor.putInt("selection-end", mRTMessageField.getSelectionEnd());

            editor.putInt("fileformat", fileformat);

            editor.putBoolean("openingIntent", openingIntent);
            editor.apply();
        }
    }

    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        newIntent(intent);
    }

    public boolean isTextChanged() {
        return textChanged;
    }

    protected void updateOptions() {
        // load the preferences
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        autoComplete = sharedPref.getBoolean("autocomplete", false);

        setContentView(mSplitToolbar ? R.layout.rte_demo_2 : R.layout.rte_demo_1);

        // initialize rich text manager
        RTApi rtApi = new RTApi(this, new RTProxyImpl(this), new RTMediaFactoryImpl(this, true));
        mRTManager = new RTManager(rtApi, mSavedInstanceState);

        ViewGroup toolbarContainer = (ViewGroup) findViewById(R.id.rte_toolbar_container);

        // register toolbar 0 (if it exists)
        RTToolbar rtToolbar0 = (RTToolbar) findViewById(R.id.rte_toolbar);
        if (rtToolbar0 != null) {
            mRTManager.registerToolbar(toolbarContainer, rtToolbar0);
        }

        // register toolbar 1 (if it exists)
        RTToolbar rtToolbar1 = (RTToolbar) findViewById(R.id.rte_toolbar_character);
        if (rtToolbar1 != null) {
            mRTManager.registerToolbar(toolbarContainer, rtToolbar1);
        }

        // register toolbar 2 (if it exists)
        RTToolbar rtToolbar2 = (RTToolbar) findViewById(R.id.rte_toolbar_paragraph);
        if (rtToolbar2 != null) {
            mRTManager.registerToolbar(toolbarContainer, rtToolbar2);
        }


        // register message editor
        mRTMessageField = (RTEditText) findViewById(R.id.rtEditText_1);
        mRTManager.registerEditor(mRTMessageField, true);
        //  if (message != null) {
        //    mRTMessageField.setRichTextEditing(true, message);
        //  }

      /*  if (autocorrect && autocase) {
            mRTMessageField.setInputType(InputType.TYPE_TEXT_FLAG_AUTO_CORRECT |
                    InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        } else if (autocorrect) {
            mRTMessageField.setInputType(InputType.TYPE_TEXT_FLAG_AUTO_CORRECT);
        } else if (autocase) {
            mRTMessageField.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        }*/

        mRTMessageField.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence one, int a, int b, int c) {

                // put a little star in the title if the file is changed
                textChanged = true;

                CharSequence temp = getTitle();
                temp = temp.toString().replace("*", "");
                setTitle("*" + temp.toString());

            }

            // complete the interface
            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
        });

        /********************************
         * show/hide filename */
        boolean value = sharedPref.getBoolean("hidefilename", false);
        if (value)
            setTitle("Note Editor");

        /********************************
         * line wrap */
       /* mRTMessageField.setHorizontallyScrolling(false);
        // setup the scroll view correctly
        ScrollView scroll = (ScrollView) findViewById(R.id.rte_content);
        if (scroll != null) {
            scroll.setFillViewport(true);
            scroll.setHorizontalScrollBarEnabled(false);
        }*/

        boolean linksclickable = sharedPref.getBoolean("links", true);
        if (linksclickable) {
            mRTMessageField.setAutoLinkMask(Linkify.ALL);
            mRTMessageField.setLinksClickable(true);
        } else
            mRTMessageField.setAutoLinkMask(0);

        mRTMessageField.requestFocus();
    }

    public static int countQuotes(String t) {
        int i = -1;
        int count = -1;

        do {
            i = t.indexOf('"', i + 1);
            count++;
        } while (i != -1);

        return count;
    }


    public void newIntent(Intent intent) {
        setIntent(intent);
        Uri mUri;
        // search action
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);

            SearchRecentSuggestions suggestions = new SearchRecentSuggestions(this,
                    SearchSuggestions.AUTHORITY, SearchSuggestions.MODE);
            suggestions.saveRecentQuery(query, null);

            int number = intent.getIntExtra("number", 0);
            number++;
            intent.putExtra("number", number);

            if (number == 1) {
                fromSearch = true;
                queryString = query;
            }
        } else if (EditorActivity.PLAIN_TEXT.equals(intent.getAction())) {
            fromIntent = true;
            mUri = intent.getData();
            if (isTextChanged()) {
                openingIntent = true;
                newIntent = intent;
                showDialog(DIALOG_SHOULD_SAVE_INTENT);
            } else {
                openFile(mUri);
                mRTMessageField.setText(intent.getStringExtra(EditorActivity.PLAIN_TEXT));
            }
        } else {

            // opening something action
            mUri = intent.getData();

            if (mUri != null) {
                myResume();

                // this is so we know if this intent was already displayed
                int number = intent.getIntExtra("number", 0);
                number++;
                intent.putExtra("number", number);
                if (!mUri.getPath().equals(filename) && number <= 1) {
                    // set stuff up
                    fromIntent = true;

                    // figure out what to do
                    if (!mUri.getPath().equals(filename) && isTextChanged()) {
                        openingIntent = true;
                        newIntent = intent;
                        showDialog(DIALOG_SHOULD_SAVE_INTENT);
                    } else if (!mUri.getPath().equals(filename)) {
                        openFile(mUri);
                    }
                }
            }
        }
    }

    public void openFile(CharSequence fname) {
        openingFile = false;
        StringBuffer result = new StringBuffer();

        try {
            // open file
            FileReader f = new FileReader(fname.toString());
            File file = new File(fname.toString());

            if (f == null) {
                throw (new FileNotFoundException());
            }

            if (file.isDirectory()) {
                throw (new IOException());
            }

            // if the file has nothing in it there will be an exception here
            // that actually isn't a problem
            if (file.length() != 0 && !file.isDirectory()) {
                char[] buffer;
                buffer = new char[1100];    // made it bigger just in case

                int read = 0;

                do {
                    read = f.read(buffer, 0, 1000);

                    if (read >= 0) {
                        result.append(buffer, 0, read);
                    }
                } while (read >= 0);
            }
        } catch (FileNotFoundException e) {
            // file not found
            errorFname = fname;
            openingError = true;
            showDialog(DIALOG_NOTFOUND_ERROR);
        } catch (IOException e) {
            // error reading file
            errorFname = fname;
            openingError = true;
            showDialog(DIALOG_READ_ERROR);
        } catch (Exception e) {
            errorFname = fname;
            openingError = true;
            showDialog(DIALOG_READ_ERROR);
        }

        // now figure out the file format, nl, cr, crnl
        if (!openingError) {
            openFile(fname, result);
        }

        errorSaving = false;
        if (mRTMessageField != null)
            mRTMessageField.requestFocus();
    }

    public void openFile(Uri mUri) {
        File ft = new File(mUri.getPath());

        if (ft.isFile()) {
            openFile(mUri.getPath());
            return;
        }

        // figure out file name
        int count = 0;
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        String location = sharedPref.getString("defaultdir", Constants.NOTE_TEXT_DIR);

        File f = new File(location + "/attachment");

        while (f.isFile()) {
            count++;
            f = new File(location + "/attachment" + count);
        }

        // now read in the file
        try {
            ParcelFileDescriptor pfd = getContentResolver().openFileDescriptor(mUri, "r");
            FileReader fis = new FileReader(pfd.getFileDescriptor());

            char[] buffer;
            buffer = new char[1100];    // made it bigger just in case

            StringBuffer result = new StringBuffer();

            int read = 0;

            do {
                read = fis.read(buffer);

                if (read >= 0) {
                    result.append(buffer, 0, read);
                }
            } while (read >= 0);

            openFile((CharSequence) f.toString(), result);
            removeRecentFile((CharSequence) f.toString());

            // indicate it hasn't been saved
            // put a little star in the title if the file is changed
            if (!isTextChanged()) {
                CharSequence temp = getTitle();
                setTitle("*" + temp.toString().substring(temp.toString().lastIndexOf("/") + 1));
            }

        } catch (Exception e) {
            // error reading file
            errorFname = "attachment";
            openingError = true;
            showDialog(DIALOG_READ_ERROR);
        }
    }

    public void openFile(CharSequence fname, StringBuffer result) {
        try {
            // have to do this first because it resets fileformat
            createNew(); // to clear everything out

            String newText = result.toString();

            if (newText.indexOf("\r\n", 0) != -1) {
                fileformat = FILEFORMAT_CRNL;
                newText = newText.replace("\r", "");
            } else if (newText.indexOf("\r", 0) != -1) {
                fileformat = FILEFORMAT_CR;
                newText = newText.replace("\r", "\n");
            } else {
                fileformat = FILEFORMAT_NL;
            }

            // Okay, now we can set everything up
            mRTMessageField.setRichTextEditing(true, newText);
            setTitle("*" + fname.toString().substring(fname.toString().lastIndexOf("/") + 1));

            File f = new File(fname.toString());
            lastModified = f.lastModified();
            filename = fname;
            untitled = false;

            addRecentFile(fname);
            openingRecent = false;

            // this is just incase we get an error
        } catch (Exception e) {
            errorFname = fname;
            openingError = true;
            showDialog(DIALOG_READ_ERROR);
        }

        openingIntent = false;
        temp_filename = "";
    }

    public void createNew() {

        setTitle(R.string.newFileName);
        mRTMessageField.setRichTextEditing(true, "");
        // clear the saved text
        SharedPreferences.Editor editor = getPreferences(MODE_PRIVATE).edit();
        if (editor != null) {
            editor.putInt("mode", 1);

            editor.putString("text", "");
            editor.putInt("text-quotes", 0);

            editor.putString("fntext", getTitle().toString());
            editor.putInt("fntext-quotes", countQuotes(getTitle().toString()));

            editor.putString("filename", "");
            editor.putInt("filename-quotes", 0);

            editor.putInt("selection-start", -1);
            editor.putInt("selection-end", -1);
            editor.apply();
        }

        fileformat = FILEFORMAT_NL;
        filename = "";
        lastModified = 0;
        untitled = true;

        creatingFile = false;

        updateOptions();
        mRTMessageField.requestFocus();

    }

    protected void addRecentFile(CharSequence f) {
        if (recentItems == null)
            readRecentFiles();

        // remove from list if it is already there
        int i;
        int length = recentItems.size();

        for (i = 0; i < length; i++) {
            String t = recentItems.get(i);
            if (t.equals(f.toString())) {
                recentItems.remove(i);
                i--;
                length--;
            }
        }

        // add the new file
        recentItems.add(0, f.toString());

        // make sure there are 7 max
        if (recentItems.size() > 7)
            recentItems.remove(7);

        // save this list in the preferences
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
        //SharedPreferences.Editor editor = getPreferences(MODE_PRIVATE).edit();

        for (i = 0; i < recentItems.size(); i++) {
            editor.putString("rf_file" + i, recentItems.get(i));
        }

        editor.putInt("rf_numfiles", recentItems.size());
        editor.apply();
    }

    protected void readRecentFiles() {
        int i;

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

        int numFiles = prefs.getInt("rf_numfiles", 0);

        // clear the current list
        if (recentItems == null)
            recentItems = new ArrayList<String>();

        recentItems.clear();

        // start adding stuff
        for (i = 0; i < numFiles; i++) {
            recentItems.add(prefs.getString("rf_file" + i, i + ""));
        }
    }

    protected void removeRecentFile(CharSequence f) {
        if (recentItems == null)
            readRecentFiles();

        // remove from list if it is already there
        int i;
        int length = recentItems.size();
        for (i = 0; i < length; i++) {
            String t = recentItems.get(i);

            if (t.equals(f.toString())) {
                recentItems.remove(i);
                i--;
                length--;
            }
        }

        // save this list in the preferences
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
        //SharedPreferences.Editor editor = getPreferences(MODE_PRIVATE).edit();

        for (i = 0; i < recentItems.size(); i++) {
            editor.putString("rf_file" + i, recentItems.get(i));
        }

        editor.putInt("rf_numfiles", recentItems.size());
        editor.apply();
    }

    protected void onPrepareDialog(int id, Dialog dialog) {
        switch (id) {
            // just update the messages for errors
            case DIALOG_NOTFOUND_ERROR:
                dialog.setTitle(errorFname + " not found");
                break;

            case DIALOG_READ_ERROR:
                dialog.setTitle("Error reading " + errorFname);
                break;

            case DIALOG_RECENT_FILE_DIALOG: {
                // make sure we have the most recent recent files in this dialog
                readRecentFiles();
                recentFilesAdapter.notifyDataSetChanged();

                openRecentListView.setSelection(0);
            }
        }
    }

    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DIALOG_RECENT_FILE_DIALOG: {
                // read the file list
                readRecentFiles();

                // update the files list
                recentFilesAdapter = new FileViewArrayAdapter(getBaseContext(), recentItems);

                // custom listview so that we can put the list back up to the top automatically
                // in the on prepare
                LayoutInflater factory = LayoutInflater.from(this);
                openRecentListView = (ListView) factory.inflate(R.layout.openrecent_list, null);


                openRecentListView.setAdapter(recentFilesAdapter);
                openRecentListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> arg0, View arg1,
                                            int arg2, long arg3) {
                        openFile(recentItems.get(arg2));
                        openRecentDialog.dismiss();
                    }
                });

                openRecentDialog = new AlertDialog.Builder(this)
                        .setIcon(R.drawable.ic_dialog_menu_generic)
                        .setTitle(R.string.openRecent)
                        .setView(openRecentListView)
                        .setInverseBackgroundForced(true)
                        .setOnCancelListener(new DialogInterface.OnCancelListener() {
                            public void onCancel(DialogInterface dialog) {
                                openingRecent = false;
                            }
                        })
                        .create();

                return openRecentDialog;
            }

            case DIALOG_SAVE_ERROR_PERMISSIONS: {
                return new AlertDialog.Builder(this)
                        .setTitle(R.string.accessDenied)
                        .setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                // what to do when positive clicked
                                if (!openingRecent) {
                                    createDialog(FileDialog.Mode.SAVE_FILE, DIALOG_SAVE_FILE);
                                }
                            }
                        })
                        .create();
            }

            case DIALOG_SAVE_ERROR_SDCARD: {
                return new AlertDialog.Builder(this)
                        .setMessage(R.string.accessDeniedSDcard)
//				.setTitle(R.string.accessDeniedSDcard)
                        .setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                // what to do when positive clicked
//						if (!openingRecent)
                                {
                                    createDialog(FileDialog.Mode.SAVE_FILE, DIALOG_SAVE_FILE);
                                }
                            }
                        })
                        .create();
            }

            case DIALOG_SAVE_ERROR: {
                return new AlertDialog.Builder(this)
                        .setMessage(R.string.savingError)
//					.setTitle(R.string.savingError)
                        .setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                // what to do when positive clicked
//							if (!openingRecent)
                                {
                                    createDialog(FileDialog.Mode.SAVE_FILE, DIALOG_SAVE_FILE);
                                }
                            }
                        })
                        .create();
            }

            case DIALOG_NOTFOUND_ERROR: {
                return new AlertDialog.Builder(this)
                        .setTitle(errorFname + " not found")
                        .setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                // what to do when positive clicked
                                // do nothing, just letting the user know
                                if (openingRecent) {
                                    removeRecentFile(errorFname);
                                } else {
                                    createDialog(FileDialog.Mode.PICK_FILE, DIALOG_OPEN_FILE);
                                }
                            }
                        })
                        .create();
            }

            case DIALOG_READ_ERROR: {
                return new AlertDialog.Builder(this)
                        .setTitle("Error reading " + errorFname)
                        .setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                // what to do when positive clicked
                                // do nothing, just letting the user know
                                if (!openingIntent) {
                                    createDialog(FileDialog.Mode.PICK_FILE, DIALOG_OPEN_FILE);
                                }
                            }
                        })
                        .create();
            }

            case DIALOG_SHOULD_SAVE_INTENT:
            case DIALOG_SHOULD_SAVE: {
                int t;
                if (id == DIALOG_SHOULD_SAVE_INTENT)
                    t = R.string.shouldSaveIntent;
                else
                    t = R.string.shouldSave;

                return new AlertDialog.Builder(this)
                        .setTitle(t)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {

                                savingFile = true;

                                if (untitled) {
                                    createDialog(FileDialog.Mode.SAVE_FILE, DIALOG_SAVE_FILE);
                                } else {
                                    saveNote(filename);

                                    if (!errorSaving && openingRecent)
                                        showDialog(DIALOG_RECENT_FILE_DIALOG);

                                    if (!errorSaving && creatingFile)
                                        createNew();

                                    if (!errorSaving && openingFile) {
                                        createDialog(FileDialog.Mode.PICK_FILE, DIALOG_OPEN_FILE);
                                    }

                                    if (!errorSaving && openingIntent) {
                                        Uri mUri;

                                        if (newIntent != null) {
                                            mUri = newIntent.getData();
                                            if (EditorActivity.PLAIN_TEXT.equals(newIntent.getAction())) {
                                                mRTMessageField.setText(newIntent.getStringExtra(EditorActivity.PLAIN_TEXT));
                                            } else openFile(mUri);
                                        } else {
                                            mUri = getIntent().getData();

                                            openFile(mUri);
                                        }
                                    }

                                    if (!errorSaving && sendingAttachment)
                                        sendAttachment();
                                }
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                sendingAttachment = false;

                                if (creatingFile)
                                    createNew();

                                if (openingRecent)
                                    showDialog(DIALOG_RECENT_FILE_DIALOG);

                                if (openingFile) {
                                    createDialog(FileDialog.Mode.PICK_FILE, DIALOG_OPEN_FILE);
                                }

                                if (openingIntent) {
                                    Uri mUri;

                                    if (newIntent != null) {
                                        mUri = newIntent.getData();
                                        if (EditorActivity.PLAIN_TEXT.equals(newIntent.getAction())) {
                                            mRTMessageField.setText(newIntent.getStringExtra(EditorActivity.PLAIN_TEXT));
                                            mUri=null;
                                        }
                                    } else
                                        mUri = getIntent().getData();

                                    if (mUri != null && !mUri.getPath().equals(filename))
                                        openFile(mUri);
                                }
                            }
                        })
                        .create();
            }
            case DIALOG_MODIFIED: {
                return new AlertDialog.Builder(this)
                        .setTitle(R.string.externalModify)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                openFile(filename);
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                lastModified = (new File(filename.toString())).lastModified();
                            }
                        })
                        .create();
            }

            case DIALOG_OVERWRITE: {
                return new AlertDialog.Builder(this)
                        .setTitle(R.string.shouldOverwrite)
                        .setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {

                                saveNote(temp_filename);

                                if (!errorSaving && openingRecent)
                                    showDialog(DIALOG_RECENT_FILE_DIALOG);

                                if (!errorSaving && creatingFile)
                                    createNew();

                                if (!errorSaving && openingFile) {
                                    createDialog(FileDialog.Mode.PICK_FILE, DIALOG_OPEN_FILE);
                                }

                                if (!errorSaving && sendingAttachment)
                                    sendAttachment();

                                if (!errorSaving && openingIntent)
                                    openFile(getIntent().getData());

                                if (!errorSaving)
                                    temp_filename = "";
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                createDialog(FileDialog.Mode.SAVE_FILE, DIALOG_SAVE_FILE);
                            }
                        })
                        .create();
            }
        }

        return null;
    }

    public void saveNote(CharSequence fname) {
        errorSaving = false;

        // actually save the file here
        try {
            File f = new File(fname.toString());

            if ((f.exists() && !f.canWrite()) || (!f.exists() && !f.getParentFile().canWrite())) {
                creatingFile = false;
                openingFile = false;
                errorSaving = true;

                if (fname.toString().indexOf("/sdcard/") == 0)
                    showDialog(DIALOG_SAVE_ERROR_SDCARD);
                else
                    showDialog(DIALOG_SAVE_ERROR_PERMISSIONS);

                mRTMessageField.requestFocus();

                f = null;
                return;
            }
            f = null; // hopefully this gets garbage collected

            // Create file
            FileWriter fstream = new FileWriter(fname.toString());
            BufferedWriter out = new BufferedWriter(fstream);

            if (fileformat == FILEFORMAT_CR) {
                out.write(mRTMessageField.getText(RTFormat.HTML).replace("\n", "\r"));
            } else if (fileformat == FILEFORMAT_CRNL) {
                out.write(mRTMessageField.getText(RTFormat.HTML).replace("\n", "\r\n"));
            } else {
                out.write(mRTMessageField.getText(RTFormat.HTML));
            }

            out.close();
            textChanged = false;
            // give a nice little message
            Toast.makeText(this, R.string.onSaveMessage, Toast.LENGTH_SHORT).show();

            // the filename is the new title
            setTitle(fname.toString().substring(fname.toString().lastIndexOf("/") + 1));
            filename = fname;
            untitled = false;

            lastModified = (new File(filename.toString())).lastModified();

            temp_filename = "";

            addRecentFile(fname);
            NoteItem a = new NoteItem("text", new File(filename.toString()).getName(), filename.toString());
            ContentValues value = a.getContentValues();
            Uri uri = getContentResolver().insert(NoteContentProvider.CONTENT_URI, value);
            getContentResolver().notifyChange(uri != null ? uri : null, null);
        } catch (Exception e) { //Catch exception if any
            creatingFile = false;
            openingFile = false;

            if (fname.toString().indexOf("/sdcard/") == 0)
                showDialog(DIALOG_SAVE_ERROR_SDCARD);
            else
                showDialog(DIALOG_SAVE_ERROR);

            errorSaving = true;
        }

        mRTMessageField.requestFocus();
    }

    public void sendAttachment() {
        Intent sIntent = new Intent(Intent.ACTION_SEND);
        sIntent.setType("message/rfc822");
        sIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + filename));
        startActivity(Intent.createChooser(sIntent, "Send attachment with:"));

        sendingAttachment = false;
    }

    // returns a file list for out autocomplete open and save dialogs
    private static File prevFile = null;

    public void getFileList(CharSequence s, AutoCompleteTextView acView) {
        File f = new File(s.toString());
        File pFile;

        // get the parent directory
        if (f.isDirectory() && s.charAt(s.length() - 1) == '/')
            pFile = f;
        else
            pFile = f.getParentFile();

        // if we have no text and give some defaults
        if (s.equals(""))
            pFile = new File("/");

        if (pFile == null)
            pFile = new File("/");

        // we the parent file is actually different then update it
        if (pFile != null && (prevFile == null || (prevFile != null && !prevFile.equals(pFile)))) {
            if (!pFile.canRead())
                items.clear();    // no permission so no items
            else {
                File[] files = new File[0];

                // get he file list if there is one
                if (pFile.isDirectory())
                    files = pFile.listFiles();

                // add all the items
                if (items == null)
                    items = new ArrayList<String>();
                else
                    items.clear();

                int i, length = files.length;
                for (i = 0; i < length; i++) {
                    if (files[i].isDirectory())
                        items.add(files[i].getPath() + "/");
                    else
                        items.add(files[i].getPath());
                }
            }

            // update the files list
            FileAutoCompleteArrayAdapter adapter = new FileAutoCompleteArrayAdapter(getBaseContext(), android.R.layout.simple_dropdown_item_1line, items);
            acView.setAdapter(adapter);
        }

        prevFile = pFile;
    }

    private void createDialog(FileDialog.Mode mode, int requestCode) {
        FileDialog f = new FileDialog();
        Bundle arguments = new Bundle();
        arguments.putInt(FileDialog.ARG_REQUEST_CODE, requestCode);
        f.setMode(mode);
        f.setTitle("Pick File");
        f.setTempDirectory(Environment.getExternalStorageDirectory().getAbsolutePath());
        f.setArguments(arguments);
        f.show(getSupportFragmentManager(), "FileDialog");
    }

    @Override
    public void onFileReturned(int requestcode, File f) {
        switch (requestcode) {
            case DIALOG_SAVE_FILE:
                boolean exists = f.exists();
                boolean isdir = f.isDirectory();
                boolean canwrite = f.canWrite();

                if (exists && !isdir && canwrite) {
                    temp_filename = f.getAbsolutePath();
                    showDialog(DIALOG_OVERWRITE);
                } else {
                    // this will handle some of the other errors.
                    saveNote(f.getAbsolutePath());

                    savingFile = false;

                    if (!errorSaving && openingRecent)
                        showDialog(DIALOG_RECENT_FILE_DIALOG);

                    if (!errorSaving && creatingFile)
                        createNew();

                    if (!errorSaving && openingFile) {
                        createDialog(FileDialog.Mode.PICK_FILE, DIALOG_OPEN_FILE);
                    }

                    if (!errorSaving && sendingAttachment)
                        sendAttachment();

                    if (!errorSaving && openingIntent && getIntent().getData() != null)
                        openFile(getIntent().getData());
                }
                fileBrowserReturnFile = f.getAbsolutePath();
                backFromFileBrowser = true;
                break;
            case DIALOG_OPEN_FILE:
                openFile(f.getAbsolutePath());
                fileBrowserReturnFile = f.getAbsolutePath();
                backFromFileBrowser = true;
                break;
        }
    }
}
