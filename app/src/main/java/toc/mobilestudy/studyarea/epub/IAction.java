package toc.mobilestudy.studyarea.epub;

/*
 * Equivalent of a C# Action
 */
public interface IAction {
    void doAction();
}
