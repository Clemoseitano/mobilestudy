package toc.mobilestudy;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Clement Osei Tano K on 18/05/2017.
 * The content here is free software unless
 * it contains parts that can be claimed to
 * be proprietary, in which case you the user
 * is required to update this header to contain
 * the appropriate license header.
 * This software is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

public class TimetableAdapter extends BaseAdapter {
    private List<TimetableItem> items = new ArrayList<>();
    private Context context;

    TimetableAdapter(Context context, List<TimetableItem> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public TimetableItem getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return null;
    }
}
