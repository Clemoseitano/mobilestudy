package toc.mobilestudy;

/**
 * Created by Clement Osei Tano K on 16/05/2017.
 * The content here is free software unless
 * it contains parts that can be claimed to
 * be proprietary, in which case you the user
 * is required to update this header to contain
 * the appropriate license header.
 * This software is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

public class TimetableDbHelper extends SQLiteOpenHelper implements BaseColumns {
    public static final String TABLE_NAME = "timetable";
    public static final String ID = "item_id";
    public static final String ITEM_NAME = "item_name";
    public static final String START_TIME = "start_time";
    public static final String END_TIME = "end_time";
    public static final String PERIOD_COUNT = "period_count";
    public static final String LOCATION = "location";
    public static final String LECTURER = "lecturer";
    public static final String ITEM_DAY = "day_of_week";
    public static final String REPEAT = "repeating";
    private static final String DB_NAME = "timetable.db";
    private static final int VERSION = 1;

    public TimetableDbHelper(Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_NAME
                + " ( " + ID + " integer primary key autoincrement, "
                + ITEM_NAME + " TEXT, "
                + START_TIME + " INTEGER, "
                + END_TIME + " INTEGER, "
                + ITEM_DAY + " INTEGER, "
                + REPEAT + " INTEGER, "
                + LECTURER + " TEXT, "
                + LOCATION + " LONGTEXT, "
                + PERIOD_COUNT + " INTEGER)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
//		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
//		onCreate(db);
    }
}
