package toc.mobilestudy;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.preference.PreferenceManager;
import android.provider.BaseColumns;
import android.util.Log;

import java.math.BigInteger;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

public class BookDbHelper extends SQLiteOpenHelper implements BaseColumns {
    public static final String KEY_ID = "_id";
    public static final String KEY_TYPE = "item_type";
    public static final String KEY_DATE_TAKEN = "date_taken";
    public static final String KEY_DATE_MODIFIED = "date_modified";
    public static final String KEY_NAME = "item_name";
    public static final String KEY_LINK = "item_link";
    public static final String KEY_HASH = "item_hash";
    public static final String DATABASE_NAME = "book.db";
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_TABLE = "book";
    public static final String DATABASE_CREATE = "create table " +
            DATABASE_TABLE + " (" + KEY_ID +
            " integer primary key autoincrement, " +
            KEY_TYPE + " text not null, " +
            KEY_DATE_TAKEN + " integer, " +
            KEY_DATE_MODIFIED + " integer, " +
            KEY_HASH + " text, " +//md5 of filename, later we do for entire file
            KEY_NAME + " text not null, " +
            KEY_LINK + " text not null);";
    private final SharedPreferences prefs;
    private SQLiteDatabase mDb;

    public BookDbHelper(Context context) {

        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
    }
    public BookDbHelper open() throws SQLException {
        mDb = this.getWritableDatabase();
        return this;
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.e("BOOK", "Creating db");
        db.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE);
    }

    public void dropTable() {
        mDb.execSQL("DROP TABLE IF EXISTS " + BookDbHelper.DATABASE_TABLE);
        mDb.execSQL(BookDbHelper.DATABASE_CREATE);
    }

    public long createEntry(String type, String name, String link) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(KEY_TYPE, type);
        initialValues.put(KEY_DATE_TAKEN, DateHelper.convertDateToString(new Date(System.currentTimeMillis())));
        initialValues.put(KEY_DATE_MODIFIED, DateHelper.convertDateToString(new Date(System.currentTimeMillis())));
        initialValues.put(KEY_NAME, name);
        initialValues.put(KEY_HASH, md5(link));
        initialValues.put(KEY_LINK, link);
        return mDb.insert(DATABASE_TABLE, null, initialValues);
    }

    public boolean deleteEntry(long rowId) {

        return mDb.delete(DATABASE_TABLE, KEY_ID + "=" + rowId, null) > 0;
    }

    public Cursor fetchAllEntries() {

        return mDb.query(DATABASE_TABLE, new String[]{KEY_ID, KEY_TYPE,
                KEY_DATE_MODIFIED,
                KEY_DATE_TAKEN,
                KEY_NAME,
                KEY_HASH,
                KEY_LINK}, null, null, null, null, prefs.getString(Constants.SORT_ORDER, KEY_NAME) + " collate nocase");
    }

    public Cursor fetchEntry(String rowname) throws SQLException {

        Cursor mCursor =

                mDb.query(true, DATABASE_TABLE, new String[]{KEY_ID, KEY_TYPE,
                                KEY_DATE_MODIFIED,
                                KEY_DATE_TAKEN,
                                KEY_NAME,
                                KEY_HASH,
                                KEY_LINK}, KEY_NAME + "='" + rowname + "'", null,
                        null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    public static String md5(String s) {
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("MD5");
            digest.update(s.getBytes(Charset.forName("US-ASCII")), 0, s.length());
            byte[] magnitude = digest.digest();
            BigInteger bi = new BigInteger(1, magnitude);
            String hash = String.format("%0" + (magnitude.length << 1) + "x", bi);
            return hash;
        } catch (NoSuchAlgorithmException ignored) {

        }
        return "xyzzy";
    }
}
