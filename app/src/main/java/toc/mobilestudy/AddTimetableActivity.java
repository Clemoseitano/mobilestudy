package toc.mobilestudy;

import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.app.Activity;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.artifex.mupdfdemo.AsyncTask;
import com.fourmob.datetimepicker.date.DatePickerDialog;
import com.sleepbot.datetimepicker.time.RadialPickerLayout;
import com.sleepbot.datetimepicker.time.TimePickerDialog;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class AddTimetableActivity extends AppCompatActivity implements TimePickerDialog.OnTimeSetListener {
    private static final String TIME_PICKER_DIALOG = "time_picker";
    public static final String ITEM_ROW = "item_id";
    private EditText mTitleView;
    private EditText mRoomView;
    private EditText mLecturerView;
    private TextView mStartView;
    private TextView mEndView;
    private ImageButton mStartButton;
    private ImageButton mEndButton;
    private Spinner mDaySpinner;
    private CheckBox mReminderCheck;
    private Button mConfirmButton;
    private TimePickerDialog timePickerDialog;

    private Long mStartTime = 1L;
    private Long mEndTime = -1L;
    private int mDay = -1;
    private int type = -1;
    private boolean editmode = false;
    private long itemRow = -1L;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_timetable);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        updateStatus();
        mTitleView = (EditText) findViewById(R.id.title);
        mRoomView = (EditText) findViewById(R.id.room_number);
        mLecturerView = (EditText) findViewById(R.id.lecturer);
        mStartView = (TextView) findViewById(R.id.start_text);
        mEndView = (TextView) findViewById(R.id.end_time);
        mStartButton = (ImageButton) findViewById(R.id.start_button);
        mEndButton = (ImageButton) findViewById(R.id.end_button);
        mDaySpinner = (Spinner) findViewById(R.id.day_spinner);
        mReminderCheck = (CheckBox) findViewById(R.id.reminder);
        mConfirmButton = (Button) findViewById(R.id.confirm);

        mStartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                type = 1;
                createTimeDialog();
            }
        });
        mEndButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                type = 2;
                createTimeDialog();
            }
        });
        mStartView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                type = 1;
                createTimeDialog();
            }
        });
        mEndView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                type = 2;
                createTimeDialog();
            }
        });

        ArrayAdapter<CharSequence> dayAdapter = ArrayAdapter.createFromResource(this,
                R.array.day_display_values, android.R.layout.simple_spinner_item);
        dayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mDaySpinner.setAdapter(dayAdapter);
        mDaySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mDay = getResources().getIntArray(R.array.day_values)[i];
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        mConfirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mTitleView.getText().toString().equals(""))
                    mTitleView.setError("This field is required");
                if (mDay == -1)
                    Toast.makeText(AddTimetableActivity.this, "Select a day to add you lesson to", Toast.LENGTH_SHORT).show();
                if (mEndTime == -1L || mStartTime == -1L)
                    Toast.makeText(AddTimetableActivity.this, "Start and end time is required", Toast.LENGTH_SHORT).show();
                if (mTitleView.getText() != null && mDay != -1 && mEndTime != -1L && mStartTime != -1L) {
                    int pc = 1;
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(AddTimetableActivity.this);
                    Long t = prefs.getLong(Constants.PERIOD_LENGTH, 60 * 60 * 1000);
                    pc = (int) ((mEndTime - mStartTime) / t);
                    String title = mTitleView.getText().toString();
                    String room = "Not Specified";
                    if (mRoomView.getText() != null)
                        room = mRoomView.getText().toString();
                    String lecturer = "Not Specified";
                    if (mLecturerView.getText() != null)
                        lecturer = mLecturerView.getText().toString();
                    ContentValues values = new ContentValues();
                    values.put(TimetableDbHelper.END_TIME, mEndTime);
                    values.put(TimetableDbHelper.ITEM_DAY, mDay);
                    values.put(TimetableDbHelper.ITEM_NAME, title);
                    values.put(TimetableDbHelper.LECTURER, lecturer);
                    values.put(TimetableDbHelper.LOCATION, room);
                    values.put(TimetableDbHelper.PERIOD_COUNT, pc);
                    values.put(TimetableDbHelper.START_TIME, mStartTime);
                    values.put(TimetableDbHelper.REPEAT, mReminderCheck.isChecked() ? 1 : 0);
                    if (!editmode)
                        getContentResolver().insert(AppContentProvider.TT_URI, values);
                    else
                        getContentResolver().update(AppContentProvider.TT_URI, values, TimetableDbHelper.ID + " = " + itemRow, null);
                    Toast.makeText(AddTimetableActivity.this, "Save is successful", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        });
        Calendar mCalendar = new GregorianCalendar();
        mCalendar.setTimeInMillis(System.currentTimeMillis());
        timePickerDialog = TimePickerDialog.newInstance(this,
                mCalendar.get(Calendar.HOUR_OF_DAY), mCalendar.get(Calendar.MINUTE), false, false);
    }

    private void createTimeDialog() {
        timePickerDialog.setVibrate(false);
        timePickerDialog.setCloseOnSingleTapMinute(false);
        timePickerDialog.show(getSupportFragmentManager(), TIME_PICKER_DIALOG);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (getIntent().hasExtra(ITEM_ROW)) {
            itemRow = getIntent().getLongExtra(ITEM_ROW, -1L);
            editmode = true;
        }
        if (editmode)
            loadFields();
    }

    private void loadFields() {
        Cursor c = getContentResolver().query(AppContentProvider.TT_URI, null, TimetableDbHelper.ID + " " + itemRow + " ", null, null);
        if (c != null && c.getCount() > 0) {
            c.moveToFirst();
            mEndTime = c.getLong(c.getColumnIndex(TimetableDbHelper.END_TIME));
            mStartTime = c.getLong(c.getColumnIndex(TimetableDbHelper.START_TIME));
            mEndView.setText(DateHelper.getTime(new Date(mEndTime), "HH:mm"));
            mStartView.setText(DateHelper.getTime(new Date(mStartTime), "HH:mm"));
            mTitleView.setText(c.getString(c.getColumnIndex(TimetableDbHelper.ITEM_NAME)));
            mLecturerView.setText(c.getString(c.getColumnIndex(TimetableDbHelper.LECTURER)));
            mRoomView.setText(c.getString(c.getColumnIndex(TimetableDbHelper.LOCATION)));
            mDaySpinner.setSelection(c.getInt(c.getColumnIndex(TimetableDbHelper.ITEM_DAY)));
            mReminderCheck.setChecked(c.getInt(c.getColumnIndex(TimetableDbHelper.REPEAT)) == 1);
            c.close();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater mi = getMenuInflater();
        mi.inflate(R.menu.menu_timetable, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //Menu
        switch (item.getItemId()) {
            //When home is clicked
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_add_course:
                //startActivity(new Intent(this, AddCourseActivity.class));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateStatus() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDefaultDisplayHomeAsUpEnabled(true);
            Window window = getWindow();
            if (Build.VERSION.SDK_INT >= 21) {
                actionBar.setElevation(0);
            }

            actionBar.setDisplayOptions(ActionBar.DISPLAY_HOME_AS_UP | ActionBar.DISPLAY_SHOW_TITLE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.setStatusBarColor(getResources().getColor(toc.mobilestudy.R.color.primary));
            }
            actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(toc.mobilestudy.R.color.colorPrimaryDark)));

        }
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
        if (type == 1) {
            mStartTime = (long) (60 * 60 * 1000 * hourOfDay + 60 * 1000 * minute);
            mStartView.setText(DateHelper.getTime(new Date(mStartTime), "HH:mm"));
            type = -1;
        }
        if (type == 2) {
            mEndTime = (long) (60 * 60 * 1000 * hourOfDay + 60 * 1000 * minute);
            mEndView.setText(DateHelper.getTime(new Date(mEndTime), "HH:mm"));
            type = -1;
        }
    }
}
