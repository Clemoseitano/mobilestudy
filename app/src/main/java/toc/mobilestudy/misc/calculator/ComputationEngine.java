package toc.mobilestudy.misc.calculator; import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import toc.mobilestudy.R;

//import calculator.*;

public class ComputationEngine extends Fragment {

    public static final int UNIQUE_ID = 1;

    final int PREV_ANSWER = 0;
    final int PREV_ENTRY = 1;
    final int EXTRA_FNS = 2;

    SharedPreferences prefs;
    OnSharedPreferenceChangeListener listener;

    TextView onTheFly;
    private CalcEditText io;
    Calculator calculator;
    private int state = 0;
    Button shift;

    static final String[] extra_fns = {"sinh", "cosh", "tanh", "arcsinh", "arccosh",
            "arctanh", "csc", "sec", "cot"};

    private static final String help_text = "<!DOCTYPE HTML>\n" +
            "<html>\n" +
            "<body>\n" +
            "<ol>\n" +
            "<p>Use the tabs at the top to switch between calculator, unit conversion, and graphing modes</p>\n" +
            "<p>Press shift for more functions</p>\n" +
            "<p>Scroll the title bar up to uncover the trig functions</p>\n" +
            "<p>1/x and % functions evaluate the current expression and then operate on the result</p>\n" +
            "<p>Copy/Paste works for numbers and expressions across all tabs</p>\n" +
            "<p>last/ans contain previous expressions and answers respectively</p>\n" +
            "<p>5E6 is equivalent to 5*10^6</p>\n" +
            "<p>Tap a previous result to insert it, or previous equation to replace the current input</p>\n" +
            "<p>Please submit bugs and feature requests to iprealme@gmail.com</p>\n" +
            "</ol>\n" +
            "</body>\n" +
            "</html>";

    AppGlobals appState;
    View v;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Configuration config = getResources().getConfiguration();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        registerPreferenceListener();


        v = inflater.inflate(R.layout.calc, container, false);


        return v;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setup();
        try {
            appState.setGlobalRounding(Integer.parseInt(prefs.getString("rounding", "6")));
        } catch (Exception e) {
        }
        appState.setGlobalAngleMode(prefs.getBoolean("angle_rad", true));

        if (savedInstanceState != null) {
            state = savedInstanceState.getInt("state");
            calculator.viewStr = savedInstanceState.getString("input_string");
            calculator.result = savedInstanceState.getString("result_string");
            if (calculator.viewStr == null) calculator.viewStr = "";
            if (calculator.result == null) calculator.result = "";
            io.setText(calculator.viewStr);
            setIndex(savedInstanceState.getInt("selection_index"));
            io.setSelection(getIndex());
            // if(savedInstanceState.getStringArrayList("old_inputs") != null) {
            // 	calculator.oldViews = savedInstanceState.getStringArrayList("old_inputs");
            // }
            // if(calculator.answers = savedInstanceState.getStringArrayList("old_answers") != null) {
            // 	calculator.answers = savedInstanceState.getStringArrayList("old_answers");
            // }
            updatePrevResults();
            onTheFly.setText(calculator.calcOnTheFly());
        }
    }

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (calculator != null) {
            updatePrevResults();
            if (io != null) {
                int ind = getIndex();
                io.setText(calculator.viewStr);
                io.setSelection(ind);
            }
            if (onTheFly != null)
                onTheFly.setText(calculator.calcOnTheFly());
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        if (calculator != null) {
            if (calculator.viewStr != null) {
                savedInstanceState.putString("input_string", calculator.viewStr);
                savedInstanceState.putString("result_string", calculator.result);
                savedInstanceState.putInt("selection_index", calculator.getViewIndex());
                savedInstanceState.putInt("state", state);
            }
            if (calculator.oldViews != null && calculator.answers != null) {
                savedInstanceState.putStringArrayList("old_inputs", calculator.oldViews);
                savedInstanceState.putStringArrayList("old_answers", calculator.answers);
            }
        }
    }

    private void setup() {
        appState = (AppGlobals) getActivity().getApplicationContext();
        this.calculator = appState.getMainCalc();
        if (calculator == null) Log.e("calculator", "null main calculator");
        this.io = (CalcEditText) v.findViewById(R.id.io);
        io.setText(calculator.viewStr);
        io.setOnClickListener(ioSelect);
        io.addTextChangedListener(ioChanged);
        io.setSelectionChangedListener(new CalcEditText.SelectionChangedListener() {
            public void onSelectionChanged(int selStart, int selEnd) {
                Log.e("calculator", selStart + " " + selEnd);
                if (selStart == selEnd) {
                    setIndex(selStart);
                    //io.setSelection(
                }
            }
        });

        onTheFly = (TextView) v.findViewById(R.id.on_the_fly);
        io.requestFocus();

        setupButtons();
    }

    private void setupButtons() {
        shift = (Button) v.findViewById(R.id.shift);
        shift.setOnClickListener(shiftBtn);
        Button n = (Button) v.findViewById(R.id.left);
        n.setOnClickListener(leftBtn);
        n = (Button) v.findViewById(R.id.right);
        n.setOnClickListener(rightBtn);
        n = (Button) v.findViewById(R.id.del);
        n.setOnClickListener(bspcBtn);
        n = (Button) v.findViewById(R.id.clr);
        n.setOnClickListener(clrBtn);
        n = (Button) v.findViewById(R.id.one);
        n.setOnClickListener(makeClickListener("1", "1"));
        n = (Button) v.findViewById(R.id.two);
        n.setOnClickListener(makeClickListener("2", "2"));
        n = (Button) v.findViewById(R.id.three);
        n.setOnClickListener(makeClickListener("3", "3"));
        n = (Button) v.findViewById(R.id.four);
        n.setOnClickListener(makeClickListener("4", "4"));
        n = (Button) v.findViewById(R.id.five);
        n.setOnClickListener(makeClickListener("5", "5"));
        n = (Button) v.findViewById(R.id.six);
        n.setOnClickListener(makeClickListener("6", "6"));
        n = (Button) v.findViewById(R.id.seven);
        n.setOnClickListener(makeClickListener("7", "7"));
        n = (Button) v.findViewById(R.id.eight);
        n.setOnClickListener(makeClickListener("8", "8"));
        n = (Button) v.findViewById(R.id.nine);
        n.setOnClickListener(makeClickListener("9", "9"));
        n = (Button) v.findViewById(R.id.zero);
        n.setOnClickListener(makeClickListener("0", "0"));
        n = (Button) v.findViewById(R.id.equals);
        n.setOnClickListener(equalsBtn);
        n = (Button) v.findViewById(R.id.point);
        n.setOnClickListener(makeClickListener(".", "."));
        n = (Button) v.findViewById(R.id.plus);
        n.setOnClickListener(makeClickListener("+", "+"));
        n = (Button) v.findViewById(R.id.minus);
        n.setOnClickListener(makeClickListener("-", "-"));
        n = (Button) v.findViewById(R.id.mult);
        n.setOnClickListener(makeClickListener("*", "*"));
        n = (Button) v.findViewById(R.id.sign);
        n.setOnClickListener(makeClickListener("-", "-"));
        n = (Button) v.findViewById(R.id.div);
        n.setOnClickListener(makeClickListener("/", "/"));
        n = (Button) v.findViewById(R.id.ln);
        n.setOnClickListener(makeFnClickListener("Log", "ln"));
        n = (Button) v.findViewById(R.id.sqr);
        n.setOnClickListener(sqrBtn);
        n = (Button) v.findViewById(R.id.pwr);
        n.setOnClickListener(makeClickListener("^", "^"));
        n = (Button) v.findViewById(R.id.lParen);
        n.setOnClickListener(makeClickListener("(", "("));
        n = (Button) v.findViewById(R.id.rParen);
        n.setOnClickListener(makeClickListener(")", ")"));
        n = (Button) v.findViewById(R.id.sin);
        n.setOnClickListener(makeFnClickListener("Sin", "sin"));
        n = (Button) v.findViewById(R.id.cos);
        n.setOnClickListener(makeFnClickListener("Cos", "cos"));
        n = (Button) v.findViewById(R.id.tan);
        n.setOnClickListener(makeFnClickListener("Tan", "tan"));
        n = (Button) v.findViewById(R.id.pie);
        n.setOnClickListener(makeClickListener("PI", "pi"));
        n = (Button) v.findViewById(R.id.E);
        n.setOnClickListener(makeClickListener("E", "E"));
        n = (Button) v.findViewById(R.id.ans);
        n.setOnClickListener(ansBtn);
        registerForContextMenu(n);
        n = (Button) v.findViewById(R.id.copy);
        n.setOnClickListener(copyBtn);
        n = (Button) v.findViewById(R.id.recip);
        n.setOnClickListener(recipBtn);

    }

    private void updateView(int ind, String ins) {
        StringBuffer st = new StringBuffer(calculator.viewStr);
        st.insert(getIndex(), ins);
        calculator.viewStr = st.toString();
        int newInd = getIndex() + ind;
        Log.e("calculator", "updateView: " + getIndex() + " " + calculator.viewStr.length());
        if (newInd > calculator.viewStr.length()) newInd = 0;
        if (newInd < 0) newInd = calculator.viewStr.length();
        io.setText(calculator.viewStr);
        setIndex(newInd);
        Log.e("calculator", "updateView: " + getIndex() + " " + calculator.viewStr.length());
        io.setSelection(getIndex());
    }

    private void setIndex(int ind) {
        calculator.setViewIndex(ind);
    }

    private int getIndex() {
        return calculator.getViewIndex();
    }

    private OnClickListener makePrevAnsListener(final int n) {
        return new OnClickListener() {
            public void onClick(View v) {
                if (calculator.answers.size() > n) {
                    String ans = calculator.lastAns(calculator.answers.size() - (n + 1));
                    updateView(ans.length(), ans);
                }
            }
        };
    }

    private OnClickListener makePrevInputListener(final int n) {
        return new OnClickListener() {
            public void onClick(View v) {
                if (calculator.oldViews.size() > n) {
                    calculator.lastInp(calculator.oldViews.size() - (n + 1));
                    io.setText(calculator.viewStr);
                    setIndex(calculator.viewStr.length());
                    io.setSelection(getIndex());
                }
            }
        };
    }

    private OnClickListener makeClickListener(final String token, final String viewString) {
        return new OnClickListener() {
            public void onClick(View v) {
                calculator.addToken(token, viewString.length(), getIndex());
                updateView(viewString.length(), viewString);
            }
        };
    }

    private void addCalcFn(String token, String viewString) {
        calculator.addToken(token, viewString.length(), getIndex());
        updateView(viewString.length(), viewString);
        calculator.addToken("(", 1, getIndex());
        updateView(1, "(");
    }

    private OnClickListener makeFnClickListener(final String token, final String viewString) {
        return new OnClickListener() {
            public void onClick(View v) {
                addCalcFn(token, viewString);
            }
        };
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuItem item;
        if (v.getId() == R.id.ans && state == 0) {
            menu.setHeaderTitle("Answers");
            for (int i = calculator.answers.size() - 1; i >= 0; i -= 1) {
                item = menu.add(PREV_ANSWER, i, 0, calculator.answers.get(i));
            }
        } else if (v.getId() == R.id.ans && state == 1) {
            menu.setHeaderTitle("Previous Entries");
            for (int i = calculator.oldViews.size() - 1; i >= 0; i -= 1) {
                item = menu.add(PREV_ENTRY, i, 0, calculator.oldViews.get(i));
            }
        }
    }

    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getGroupId()) {
            case PREV_ANSWER:
                String ans = calculator.lastAns(item.getItemId());
                updateView(ans.length(), ans);
                break;
            case PREV_ENTRY:
                calculator.lastInp(item.getItemId());
                io.setText(calculator.viewStr);
                setIndex(calculator.viewStr.length());
                io.setSelection(getIndex());
                break;
        }
        return true;
    }

    public void updateCalcState() {
        if (state == 1) {
            Button n = (Button) v.findViewById(R.id.del);
            n.setOnClickListener(delBtn);
            n.setText("del");
            n = (Button) v.findViewById(R.id.sin);
            n.setOnClickListener(makeFnClickListener("Arcsin", "arcsin"));
            n.setText("asin");
            n = (Button) v.findViewById(R.id.cos);
            n.setOnClickListener(makeFnClickListener("Arccos", "arccos"));
            n.setText("acos");
            n = (Button) v.findViewById(R.id.tan);
            n.setOnClickListener(makeFnClickListener("Arctan", "arctan"));
            n.setText("atan");
            n = (Button) v.findViewById(R.id.copy);
            n.setOnClickListener(pasteBtn);
            n.setText("paste");
            n = (Button) v.findViewById(R.id.sqr);
            n.setOnClickListener(makeFnClickListener("Sqrt", "sqrt"));
            n.setText("sqrt");
            n = (Button) v.findViewById(R.id.pie);
            n.setOnClickListener(makeClickListener("i", "i"));
            n.setText("i");
            n = (Button) v.findViewById(R.id.ans);
            n.setOnClickListener(ansBtn);
            n.setText("last");
            n = (Button) v.findViewById(R.id.E);
            n.setOnClickListener(makeClickListener("e", "e"));
            n.setText("e");
            n = (Button) v.findViewById(R.id.recip);
            n.setOnClickListener(percentBtn);
            n.setText("%");
            shift.setTextColor(getColor(R.color.colorAccent));
        } else {
            Button n = (Button) v.findViewById(R.id.del);
            n.setText("bspc");
            n.setOnClickListener(bspcBtn);
            n = (Button) v.findViewById(R.id.sin);
            n.setOnClickListener(makeFnClickListener("Sin", "sin"));
            n.setText("sin");
            n = (Button) v.findViewById(R.id.cos);
            n.setOnClickListener(makeFnClickListener("Cos", "cos"));
            n.setText("cos");
            n = (Button) v.findViewById(R.id.tan);
            n.setOnClickListener(makeFnClickListener("Tan", "tan"));
            n.setText("tan");
            n = (Button) v.findViewById(R.id.copy);
            n.setOnClickListener(copyBtn);
            n.setText("copy");
            n = (Button) v.findViewById(R.id.sqr);
            n.setOnClickListener(sqrBtn);
            n.setText("sqr");
            n = (Button) v.findViewById(R.id.pie);
            n.setOnClickListener(makeClickListener("PI", "pi"));
            n.setText("pi");
            n = (Button) v.findViewById(R.id.ans);
            n.setOnClickListener(ansBtn);
            n.setText("ans");
            n = (Button) v.findViewById(R.id.E);
            n.setOnClickListener(makeClickListener("E", "E"));
            n.setText("E");
            n = (Button) v.findViewById(R.id.recip);
            n.setOnClickListener(recipBtn);
            n.setText("1/x");
            shift.setTextColor(getColor(R.color.white));
        }
    }

    // TODO -- abstract (+ GraphFunctionsEntry => AppGlobals or singleton)
    private int getColor(int res) {
        return getActivity().getResources().getColor(res);
    }

    private OnClickListener shiftBtn = new OnClickListener() {
        public void onClick(View v) {
            state = (state == 0) ? 1 : 0;
            updateCalcState();
        }
    };

    private OnLongClickListener defaultLongClick = new OnLongClickListener() {
        public boolean onLongClick(View v) {
            return true;
        }
    };

    private OnClickListener ioSelect = new OnClickListener() {
        public void onClick(View v) {
            //setIndex (((TextView)v).getSelectionStart ());
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
                    Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(io.getWindowToken(), 0);
        }
    };

    private OnClickListener leftBtn = new OnClickListener() {
        public void onClick(View v) {
            setIndex(getIndex() - calculator.bspcHelper(getIndex(), false));
            if (getIndex() < 0) setIndex(calculator.viewStr.length());
            io.setSelection(getIndex());
        }
    };

    private OnClickListener rightBtn = new OnClickListener() {
        public void onClick(View v) {
            setIndex(getIndex() + calculator.delHelper(getIndex(), false));
            if (getIndex() > calculator.viewStr.length()) setIndex(0);
            io.setSelection(getIndex());
        }
    };

    private OnClickListener delBtn = new OnClickListener() {
        public void onClick(View v) {
            if (getIndex() < calculator.viewStr.length()) {
                int delNum = calculator.delHelper(getIndex(), true);
                int ind = getIndex();
                calculator.viewStr = calculator.viewStr.substring(0, getIndex()) +
                        calculator.viewStr.substring(getIndex() + delNum, calculator.viewStr.length());
                io.setText(calculator.viewStr);
                io.setSelection(ind);
            }
        }
    };

    private OnClickListener bspcBtn = new OnClickListener() {
        public void onClick(View v) {
            if (getIndex() > 0) {
                int bspcNum = calculator.bspcHelper(getIndex(), true);
                calculator.viewStr = calculator.viewStr.substring(0, getIndex() - bspcNum) +
                        calculator.viewStr.substring(getIndex(), calculator.viewStr.length());
                int newInd = getIndex() - bspcNum;
                io.setText(calculator.viewStr);
                setIndex(newInd);
                io.setSelection(getIndex());
            }
        }
    };

    private OnClickListener clrBtn = new OnClickListener() {
        public void onClick(View v) {
            calculator.tokens.clear();
            calculator.tokenLens.clear();
            calculator.viewStr = "";
            setIndex(0);
            io.setText(calculator.viewStr);
            io.setSelection(getIndex());
        }
    };

    private OnClickListener sqrBtn = new OnClickListener() {
        public void onClick(View v) {
            calculator.addToken("^", 1, getIndex());
            calculator.addToken("2", 1, getIndex() + 1);
            updateView(2, "^2");
        }
    };

    private OnClickListener equalsBtn = new OnClickListener() {
        public void onClick(View v) {
            // send to parser
            if (calculator.viewStr != null && calculator.viewStr.length() > 0) {
                if (calculator.execute()) {
                    setIndex(0);
                    io.setText(calculator.viewStr);
                    io.setSelection(getIndex());
                    onTheFly.setText(calculator.result);
                    updatePrevResults();
                } else {
                    Toast.makeText(getActivity(), "Error: Could not calculate", Toast.LENGTH_SHORT).show();
                }
            }
        }
    };

    public void updatePrevResults() {
        int n;
        if (calculator.answers.size() > calculator.oldViews.size()) {
            n = calculator.oldViews.size() - 1;
        } else {
            n = calculator.answers.size() - 1;
        }
    }

    private OnClickListener ansBtn = new OnClickListener() {
        public void onClick(View v) {
            getActivity().openContextMenu(v.findViewById(R.id.ans));
        }
    };

    private OnClickListener copyBtn = new OnClickListener() {
        public void onClick(View v) {
            appState.setCopy(calculator);
        }
    };

    private OnClickListener pasteBtn = new OnClickListener() {
        public void onClick(View v) {
            //calculator.tokenize(copy);
            appState.getCopy(calculator);
            io.setText(calculator.viewStr);
            setIndex(calculator.viewStr.length());
            io.setSelection(getIndex());
        }
    };

    private OnClickListener recipBtn = new OnClickListener() {
        public void onClick(View v) {
            calculator.tokens.add(0, new Primitive("("));
            calculator.tokenLens.add(0, 1);
            calculator.tokens.add(0, new Primitive("/"));
            calculator.tokens.add(0, new ComplexNumber(1.0, 0));
            calculator.tokenLens.add(0, 1);
            calculator.tokenLens.add(0, 1);
            calculator.tokens.add(new Primitive(")"));
            calculator.tokenLens.add(1);
            calculator.viewStr = "1/(" + calculator.viewStr + ")";
            calculator.execute();
            setIndex(calculator.viewStr.length());
            io.setText(calculator.viewStr);
            io.setSelection(getIndex());
            onTheFly.setText(calculator.result);
            updatePrevResults();
        }
    };

    private OnClickListener percentBtn = new OnClickListener() {
        public void onClick(View v) {
            calculator.tokens.add(0, new Primitive("("));
            calculator.tokenLens.add(0, 1);
            calculator.tokens.add(new Primitive(")"));
            calculator.tokenLens.add(1);
            calculator.tokens.add(new Primitive("/"));
            calculator.tokenLens.add(1);
            calculator.tokens.add(new ComplexNumber(100.0, 0));
            calculator.tokenLens.add(3);
            calculator.viewStr = "(" + calculator.viewStr + ")/100.0";
            calculator.execute();
            setIndex(calculator.viewStr.length());
            io.setText(calculator.viewStr);
            io.setSelection(getIndex());
            onTheFly.setText(calculator.result);
            updatePrevResults();
        }
    };

    private TextWatcher ioChanged = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            String fly = calculator.calcOnTheFly();
            if (fly.length() > 0 && calculator.viewStr.length() != 0) {
                onTheFly.setText(fly);
            }
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
    };

    void reportListDialogResult(int ind) {
        addCalcFn(extra_fns[ind], extra_fns[ind]);
    }


    public void help() {
        final TextView noteName = new TextView(getContext());
        final ScrollView container = new ScrollView(getContext());
        container.setBackgroundColor(getResources().getColor(R.color.colorPrimaryTransparent));
        noteName.setPadding(5, 5, 5, 5);
        noteName.setText(Html.fromHtml(help_text));
        noteName.setTextColor(getResources().getColor(R.color.white));
        container.addView(noteName);
        new AlertDialog.Builder(getContext())
                .setTitle("Help")
                .setView(container)
                .setOnKeyListener(new DialogInterface.OnKeyListener() {
                    @Override
                    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                        ArrayList<Integer> keys = new ArrayList<>();
                        keys.add(KeyEvent.KEYCODE_DPAD_CENTER);
                        keys.add(KeyEvent.KEYCODE_ENTER);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                            keys.add(KeyEvent.KEYCODE_ESCAPE);
                        }
                        keys.add(KeyEvent.KEYCODE_BACK);
                        if (event.getAction() == KeyEvent.ACTION_DOWN && keys.contains(keyCode))
                            dialog.dismiss();
                        return false;
                    }
                })
                .show();
    }
    
    
    private void showExtraFnsMenu(FragmentManager fm) {
        AppGlobals.showListDialog(fm, 0, "Extra Functions", extra_fns);
    }

    private void registerPreferenceListener() {
        listener = new OnSharedPreferenceChangeListener() {
            public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
                if ("rounding".equals(key)) {
                    try {
                        int newRnd = Integer.parseInt(prefs.getString(key, "6"));
                        if (newRnd > 0 && newRnd <= 12)
                            appState.setGlobalRounding(newRnd);
                    } catch (Exception e) {
                    }
                } else if ("angle_rad".equals(key)) {
                    appState.setGlobalAngleMode(prefs.getBoolean(key, false));
                }
            }
        };
        prefs.registerOnSharedPreferenceChangeListener(listener);
    }

    public void showOptionsMenu() {
        Intent intent = new Intent(v.getContext(), Settings.class);
        startActivityForResult(intent, 0);
    }

    public void handleOptionsItemSelected(FragmentManager fm, int itemId) {
        if (itemId == R.id.calc_help) {
            help();
        } else if (itemId == R.id.extra_fns) {
            showExtraFnsMenu(fm);
        } else if (itemId == R.id.settings) {
            showOptionsMenu();
        }
    }
}
