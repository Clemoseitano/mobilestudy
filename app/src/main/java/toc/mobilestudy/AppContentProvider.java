package toc.mobilestudy;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import java.math.BigInteger;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class AppContentProvider extends ContentProvider {
    public static final Uri BOOK_URI = Uri.parse("content://toc.mobilestudy/book");
    public static final Uri FAV_URI = Uri.parse("content://toc.mobilestudy/fav");
    public static final Uri TT_URI = Uri.parse("content://toc.mobilestudy/timetable");

    private SQLiteDatabase fav_db;
    private SQLiteDatabase book_db;
    private SQLiteDatabase tt_db;

    @Override
    public boolean onCreate() {
        fav_db = new FavDbHelper(getContext()).getWritableDatabase();
        book_db = new BookDbHelper(getContext()).getWritableDatabase();
        tt_db = new TimetableDbHelper(getContext()).getWritableDatabase();
        return (fav_db != null && book_db != null && tt_db != null);
    }

    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor c = null;
        if (sortOrder == null || sortOrder.isEmpty())
            sortOrder = FavDbHelper.KEY_NAME;
        if (uri.equals(FAV_URI))
            c = fav_db.query(FavDbHelper.DATABASE_TABLE, projection, selection, selectionArgs, null,
                    null, sortOrder + " collate nocase");
        if (uri.equals(BOOK_URI))
            c = book_db.query(BookDbHelper.DATABASE_TABLE, projection, selection, selectionArgs, null,
                    null, sortOrder + " collate nocase");
        if (uri.equals(TT_URI))
            c = tt_db.query(TimetableDbHelper.TABLE_NAME, projection, selection, selectionArgs, null,
                    null, sortOrder);
        if (c != null) {
            c.setNotificationUri(getContext().getContentResolver(), uri);
        }
        return c;
    }

    @Override
    public Uri insert(@NonNull Uri url, ContentValues values) {
        if (url.equals(FAV_URI)) {
            long rowId = fav_db.insert(FavDbHelper.DATABASE_TABLE, null, values);

            if (rowId > 0) {
                Uri uri = ContentUris.withAppendedId(FAV_URI, rowId);
                getContext().getContentResolver().notifyChange(uri, null);
                return uri;
            } else {
                Log.d("Db Conflict", "Failed to insert row into " + url);
                return null;
            }
        } else if (url.equals(BOOK_URI)) {
            values.put(BookDbHelper.KEY_HASH, md5(values.getAsString(BookDbHelper.KEY_LINK)));
            long rowId = book_db.insert(BookDbHelper.DATABASE_TABLE, null, values);
            if (rowId > 0) {
                Uri uri = ContentUris.withAppendedId(BOOK_URI, rowId);
                getContext().getContentResolver().notifyChange(uri, null);
                return uri;
            } else {
                Log.d("Db Conflict", "Failed to insert row into " + url);
                return null;
            }
        } else if (url.equals(TT_URI)) {
            long rowId = tt_db.insert(TimetableDbHelper.TABLE_NAME, null, values);
            if (rowId > 0) {
                Uri uri = ContentUris.withAppendedId(TT_URI, rowId);
                getContext().getContentResolver().notifyChange(uri, null);
                return uri;
            } else {
                Log.d("Db Conflict", "Failed to insert row into " + url);
                return null;
            }
        } else return null;
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int retVal = 0;
        if (uri.equals(FAV_URI))
            retVal = fav_db.update(FavDbHelper.DATABASE_TABLE, values, selection, selectionArgs);
        else if (uri.equals(BOOK_URI))
            retVal = book_db.update(BookDbHelper.DATABASE_TABLE, values, selection, selectionArgs);
        else if (uri.equals(TT_URI))
            retVal = tt_db.update(TimetableDbHelper.TABLE_NAME, values, selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return retVal;
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        int retVal = 0;
        if (uri.equals(FAV_URI))
            retVal = fav_db.delete(FavDbHelper.DATABASE_TABLE, selection, selectionArgs);
        else if (uri.equals(BOOK_URI))
            retVal = book_db.delete(BookDbHelper.DATABASE_TABLE, selection, selectionArgs);
        else if (uri.equals(TT_URI))
            retVal = tt_db.delete(TimetableDbHelper.TABLE_NAME, selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return retVal;
    }

    @Override
    public String getType(@NonNull Uri uri) {
        if (uri.equals(FAV_URI)) return "fav";
        else if (uri.equals(BOOK_URI)) return "book";
        else if (uri.equals(TT_URI)) return "timetable";
        return null;
    }

    public static String md5(String s) {
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("MD5");
            digest.update(s.getBytes(Charset.forName("US-ASCII")), 0, s.length());
            byte[] magnitude = digest.digest();
            BigInteger bi = new BigInteger(1, magnitude);
            String hash = String.format("%0" + (magnitude.length << 1) + "x", bi);
            return hash;
        } catch (NoSuchAlgorithmException ignored) {

        }
        return "xyzzy";
    }
}
