package toc.mobilestudy.misc.calculator; import toc.mobilestudy.R;

import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;

public class Settings extends PreferenceActivity {

    static final int BACK_ID=1;
    private SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        addPreferencesFromResource(R.xml.settings_calculator);
        findPreference("angle_rad").setSummary(prefs.getBoolean("angle_rad", false)?
                    "Toggle radian to degree":"Toggle degree to radian");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
	menu.add(0, BACK_ID, 0, "Back")
	    .setIcon(R.drawable.ic_left);
	return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
	case BACK_ID:
	    finish();
	    break;
	}
	return true;
    }


}
