package toc.mobilestudy.studyarea;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

import toc.mobilestudy.R;

public class NoteItemAdapter extends BaseAdapter {
    private final List<NoteItem> items;
    private final Context context;

    public NoteItemAdapter(Context context, List items) {
        this.items = items;
        this.context = context;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
       /* if (convertView == null || convertView instanceof TextView || convertView.getClass().getName().
                equalsIgnoreCase("android.widget.TextView")) {*/
        convertView = View.inflate(context, R.layout.note_item2, null);
        viewHolder = new ViewHolder();
        viewHolder.iconView = (ImageView) convertView.findViewById(R.id.iconView);
        viewHolder.nameView = (TextView) convertView.findViewById(R.id.nameView);
        viewHolder.modifiedView = (TextView) convertView.findViewById(R.id.modifiedView);
        viewHolder.typeView = (TextView) convertView.findViewById(R.id.typeView);
        convertView.setTag(viewHolder);

        if (items.get(position).getType().equals("separator")) {
            convertView = View.inflate(context, R.layout.separator, null);
            viewHolder.nameView = (TextView) convertView.findViewById(R.id.empty);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                viewHolder.nameView.setLetterSpacing(0.10f);
            }
            viewHolder.nameView.setText(items.get(position).getName());
            convertView.setTag(viewHolder);
            return convertView;
        }
        viewHolder.nameView.setText(items.get(position).getName());
        viewHolder.modifiedView.setText(context.getString(R.string.modified)+items.get(position).getDateModified());
        viewHolder.setIcon(items.get(position).getType());
        viewHolder.setType(items.get(position).getType());
        return convertView;
    }

    private HashMap<Integer, Boolean> mSelection = new HashMap<Integer, Boolean>();

    public void setNewSelection(int position, boolean value) {
        mSelection.put(position, value);
        notifyDataSetChanged();
    }

    public boolean isPositionChecked(int position) {
        Boolean result = mSelection.get(position);
        return result == null ? false : result;
    }

    public Set<Integer> getCurrentCheckedPosition() {
        return mSelection.keySet();
    }

    public void removeSelection(int position) {
        mSelection.remove(position);
        notifyDataSetChanged();
    }

    public void clearSelection() {
        mSelection = new HashMap<Integer, Boolean>();
        notifyDataSetChanged();
    }

    static class ViewHolder {
        ImageView iconView;
        TextView nameView;
        TextView modifiedView;
        TextView typeView;

        public void setIcon(String type) {
            if (type.contains("web"))
                iconView.setImageResource(R.drawable.ic_web);
            else if (type.contains("epub"))
                iconView.setImageResource(R.drawable.ic_web);
            else if (type.contains("pdf"))
                iconView.setImageResource(R.drawable.ic_pdf);
            else
                iconView.setImageResource(R.drawable.ic_unknown);
        }

        public void setType(String type) {
            String extension;
            if (type.contains("web"))
                extension = "web";
            else if (type.contains("epub"))
                extension = "epub";
            else if (type.contains("pdf"))
                extension = "pdf";
            else
                extension = "unknown";
            typeView.setText(extension.toUpperCase());
        }
    }
}