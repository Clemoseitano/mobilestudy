package toc.mobilestudy.misc.calculator; import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;

import toc.mobilestudy.R;

public class GraphActivity extends AppCompatActivity {
    private GraphView graph;
    private Dialog zeros;
    private ArrayAdapter<String> zerosArr;
    private static final String help_text = "\n" +
            "\t\t\t\n" +
            "\t\t\t<!DOCTYPE HTML>\n" +
            "\n" +
            "<html>\n" +
            "\t\n" +
            "\t<body>\n" +
            "\t<ol>\n" +
            "\t<p>Press and drag to move the graph</p>\n" +
            "\t<p>Pinch to zoom</p>\n" +
            "\t<p>The menu option takes you to the function entry screen</p>\n" +
            "\t<p>Reset menu option will reset the bounds to origin</p>\n" +
            "\t<p>Zeros menu option will bring up a list of all the zeros on the screen</p>\n" +
            "\t<p>Trace menu option switches from regular graphing to trace mode. Press again to switch back</p>\n" +
            "\t<p>Press <strong>Back</strong> to dismiss this list</p>\n" +
            "\t<p>Contact us through studycafe2016@gmail.com</p>\n" +
            "\t</ol>\n" +
            "\t</body></html>";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        updateStatus();

        graph = (GraphView) findViewById(R.id.graph);
        AppGlobals appState = (AppGlobals) getApplicationContext();
        appState.setGraphView(graph);

        zerosArr = new ArrayAdapter<>(this, R.layout.list_item);
        zeros = new Dialog(this);
        zeros.setContentView(R.layout.zeros_disp);
        zeros.setTitle("Zeros (Y Intercepts)");
        zeros.setCancelable(true);
    }


    private void updateStatus() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) { actionBar.setDefaultDisplayHomeAsUpEnabled(true);
            Window window = getWindow();
            if (Build.VERSION.SDK_INT >= 21) {
                actionBar.setElevation(0);
            }

            actionBar.setDisplayOptions(ActionBar.DISPLAY_HOME_AS_UP|ActionBar.DISPLAY_SHOW_TITLE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.setStatusBarColor(getResources().getColor(R.color.primary));
            }
            actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimaryDark)));

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.graph_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.menu_reset:
                graph.reset();
                break;
            case R.id.menu_functions:
                graph.graphMode = graph.GRAPH;
                Intent myIntent = new Intent(this, GraphFunctionsEntry.class);
                startActivityForResult(myIntent, 0);
                break;
            case R.id.menu_zeros:
                zerosArr.clear();
                graph.zeros(zerosArr);
                ListView lst = (ListView) zeros.findViewById(R.id.zeros_disp);
                lst.setAdapter(zerosArr);
                zeros.show();
                break;
            case R.id.menu_trace:
                graph.trace();
                break;
            case R.id.graph_help:
                help();
                break;
            case R.id.settings:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void help() {
        final TextView noteName = new TextView(GraphActivity.this);
        final ScrollView container = new ScrollView(GraphActivity.this);
        container.setBackgroundColor(getResources().getColor(R.color.colorPrimaryTransparent));
        noteName.setPadding(5, 5, 5, 5);
        noteName.setText(Html.fromHtml(help_text));
        noteName.setTextColor(getResources().getColor(R.color.white));
        container.addView(noteName);
        new AlertDialog.Builder(GraphActivity.this)
                .setTitle("Help")
                .setView(container)
                .setOnKeyListener(new DialogInterface.OnKeyListener() {
                    @Override
                    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                        ArrayList<Integer> keys = new ArrayList<>();
                        keys.add(KeyEvent.KEYCODE_DPAD_CENTER);
                        keys.add(KeyEvent.KEYCODE_ENTER);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                            keys.add(KeyEvent.KEYCODE_ESCAPE);
                        }
                        keys.add(KeyEvent.KEYCODE_BACK);
                        if (event.getAction() == KeyEvent.ACTION_DOWN && keys.contains(keyCode))
                            dialog.dismiss();
                        return false;
                    }
                })
                .show();
    }

}
