package toc.mobilestudy.misc.calculator; import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

import toc.mobilestudy.R;

/**
 * A layout that places children in an evenly distributed grid based on the specified
 * columnCount and rowCount attributes.
 */
public class EvenGridLayout extends ViewGroup {
    private int mRowCount;
    private int mColumnCount;
    private boolean mInheritParent = false;

    public EvenGridLayout(Context context) {
        this(context, null);
    }

    public EvenGridLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public EvenGridLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        final TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.EvenGridLayout, defStyle, 0);
        mRowCount = a.getInt(R.styleable.EvenGridLayout_rowCount, 1);
        mColumnCount = a.getInt(R.styleable.EvenGridLayout_columnCount, 1);
        mInheritParent = a.getBoolean(R.styleable.EvenGridLayout_inheritParent, mInheritParent);
        a.recycle();
    }

    @Override
    public boolean shouldDelayChildPressedState() {
        return false;
    }

    @SuppressWarnings("SuspiciousNameCombination")
    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        final int paddingLeft = getPaddingLeft();
        final int paddingRight = getPaddingRight();
        final int paddingTop = getPaddingTop();
        final int paddingBottom = getPaddingBottom();
        final boolean isRTL;
        isRTL = android.os.Build.VERSION.SDK_INT >= 17 && getLayoutDirection() == LAYOUT_DIRECTION_RTL;
        final int columnWidth =
                Math.round((float) (right - left - paddingLeft - paddingRight)) / mColumnCount;
        final int rowHeight =
                Math.round((float) (bottom - top - paddingTop - paddingBottom)) / mRowCount;
        int rowIndex = 0, columnIndex = 0;
        for (int childIndex = 0; childIndex < getChildCount(); ++childIndex) {
            final View childView = getChildAt(childIndex);
            if (childView.getVisibility() == View.GONE) {
                continue;
            }
            final MarginLayoutParams lp = (MarginLayoutParams) childView.getLayoutParams();
            int childTop = paddingTop + lp.topMargin + rowIndex * rowHeight;
            int childBottom = childTop - lp.topMargin - lp.bottomMargin + rowHeight;
            int childLeft = paddingLeft + lp.leftMargin +
                    (isRTL ? (mColumnCount - 1) - columnIndex : columnIndex) * columnWidth;
            int childRight = childLeft - lp.leftMargin - lp.rightMargin + columnWidth;
            int childWidth = childRight - childLeft;
            int childHeight = childBottom - childTop;

            // Make the children square
            if (childWidth > childHeight) {
                childLeft += (childWidth - childHeight) / 2;
                childWidth = childHeight;
                childRight = childLeft + childWidth;
            } else {
                childTop += (childHeight - childWidth) / 2;
                childHeight = childWidth;
                childBottom = childTop + childHeight;
            }

            if (childWidth != childView.getMeasuredWidth() ||
                    childHeight != childView.getMeasuredHeight()) {
                childView.measure(
                        MeasureSpec.makeMeasureSpec(childWidth, MeasureSpec.EXACTLY),
                        MeasureSpec.makeMeasureSpec(childHeight, MeasureSpec.EXACTLY));
            }
            childView.layout(childLeft, childTop, childRight, childBottom);
            rowIndex = (rowIndex + (columnIndex + 1) / mColumnCount) % mRowCount;
            columnIndex = (columnIndex + 1) % mColumnCount;
        }
    }

    public int getRows() {
        return mRowCount;
    }

    public int getColumns() {
        return mColumnCount;
    }

    public void setRowCount(int c) {
        mRowCount = c;
    }

    public void setColumnCount(int c) {
        mColumnCount = c;
    }

    @Override
    public LayoutParams generateLayoutParams(AttributeSet attrs) {
        return new MarginLayoutParams(getContext(), attrs);
    }

    @Override
    protected LayoutParams generateDefaultLayoutParams() {
        return new MarginLayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
    }

    @Override
    protected LayoutParams generateLayoutParams(LayoutParams p) {
        return new MarginLayoutParams(p);
    }

    @Override
    protected boolean checkLayoutParams(LayoutParams p) {
        return p instanceof MarginLayoutParams;
    }
}

