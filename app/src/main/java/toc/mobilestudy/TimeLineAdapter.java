package toc.mobilestudy;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import toc.mobilestudy.timelineview.TimelineView;
import toc.mobilestudy.utils.VectorDrawableUtils;

/**
 * Created by HP-HP on 05-12-2015.
 * The content here is free software unless
 * it contains parts that can be claimed to
 * be proprietary, in which case you the user
 * is required to update this header to contain
 * the appropriate license header.
 * This software is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */
public class TimeLineAdapter extends RecyclerView.Adapter<TimeLineAdapter.TimeLineViewHolder> {

    private List<TimetableItem> mFeedList;
    private Context mContext;
    private TimelineView.Orientation mOrientation;
    private boolean mWithLinePadding;
    private LayoutInflater mLayoutInflater;
    private OnItemClickListener mItemClickListener;

    public TimeLineAdapter(List<TimetableItem> feedList, TimelineView.Orientation orientation, boolean withLinePadding) {
        mFeedList = feedList;
        mOrientation = orientation;
        mWithLinePadding = withLinePadding;
    }

    @Override
    public int getItemViewType(int position) {
        return TimelineView.getTimeLineViewType(position, getItemCount());
    }

    public TimetableItem getItem(int p){
        return mFeedList.get(p);
    }

    @Override
    public TimeLineViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        mLayoutInflater = LayoutInflater.from(mContext);
        View view;

        if (mOrientation == TimelineView.Orientation.HORIZONTAL) {
            view = mLayoutInflater.inflate(mWithLinePadding ? R.layout.item_timeline_horizontal_line_padding : R.layout.item_timeline_horizontal, parent, false);
        } else {
            view = mLayoutInflater.inflate(mWithLinePadding ? R.layout.item_timeline_line_padding : R.layout.item_timeline, parent, false);
        }

        return new TimeLineViewHolder(view, viewType);
    }

    @Override
    public void onBindViewHolder(TimeLineViewHolder holder, int position) {

        TimetableItem timetableItem = mFeedList.get(position);
        if (timetableItem.getItemId() == -1L) {
            Calendar cal = new GregorianCalendar();
            cal.setTimeInMillis(System.currentTimeMillis());
            int i = cal.get(Calendar.DAY_OF_WEEK);
            if (timetableItem.getDay() == i - 1) {
                holder.mTimelineView.setMarker(VectorDrawableUtils.getDrawable(mContext, R.drawable.ic_day_present, R.color.colorPrimary));
                holder.mTitle.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
            } else {
                holder.mTimelineView.setMarker(VectorDrawableUtils.getDrawable(mContext, R.drawable.ic_day_future, android.R.color.darker_gray));
                holder.mTitle.setTextColor(ContextCompat.getColor(mContext, android.R.color.darker_gray));
            }
            holder.mTime.setVisibility(View.GONE);
            holder.mTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 24);
            holder.mTitle.setText(timetableItem.getTitle());
        } else {
            long time = DateHelper.getTime();
            if (time >= timetableItem.getStartTime() && time <= timetableItem.getEndTime()) {
                holder.mTimelineView.setMarker(VectorDrawableUtils.getDrawable(mContext, R.drawable.ic_active, R.color.colorPrimary));
            } else if (time > timetableItem.getStartTime() && time > timetableItem.getEndTime()) {
                holder.mTimelineView.setMarker(VectorDrawableUtils.getDrawable(mContext, R.drawable.ic_past, android.R.color.darker_gray));
            } else if (time < timetableItem.getStartTime()) {
                holder.mTimelineView.setMarker(VectorDrawableUtils.getDrawable(mContext, R.drawable.ic_future, android.R.color.darker_gray));
            }

            holder.mTime.setVisibility(View.VISIBLE);
            holder.mTime.setText(DateHelper.getTime(new Date(timetableItem.getStartTime()), "hh:mm a"));
            holder.mTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
            holder.mTitle.setTextColor(ContextCompat.getColor(mContext, R.color.black));
            holder.mTitle.setText(timetableItem.getTitle());
        }
    }

    @Override
    public int getItemCount() {
        return (mFeedList != null ? mFeedList.size() : 0);
    }

    public interface OnItemClickListener {
        public void onItemClick(View view , int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public class TimeLineViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView mTime;
        TextView mTitle;
        TimelineView mTimelineView;


        public TimeLineViewHolder(View itemView, int viewType) {
            super(itemView);
            mTime = (TextView) itemView.findViewById(R.id.text_timeline_date);
            mTitle = (TextView) itemView.findViewById(R.id.text_timeline_title);
            mTimelineView = (TimelineView) itemView.findViewById(R.id.time_marker);
            mTimelineView.initLine(viewType);
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v, getPosition());
            }
        }
    }
}
