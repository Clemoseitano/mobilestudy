package toc.mobilestudy.misc.calculator; import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import toc.mobilestudy.R;

/**
 * Created by user on 6/9/2016.
 * If this file contains no copyrighted material,
 * then feel free to call it yours.
 */


public class CalculatorActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private TabLayout tabLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        updateStatus();

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void updateStatus() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) { actionBar.setDefaultDisplayHomeAsUpEnabled(true);
            Window window = getWindow();
            if (Build.VERSION.SDK_INT >= 21) {
                actionBar.setElevation(0);
            }

            actionBar.setDisplayOptions(ActionBar.DISPLAY_HOME_AS_UP|ActionBar.DISPLAY_SHOW_TITLE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.setStatusBarColor(getResources().getColor(R.color.primary));
            }
            actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimaryDark)));

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.calc_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        int tab = tabLayout.getSelectedTabPosition();
        switch (tab) {
            case 0:
                menu.clear();
                getMenuInflater().inflate(R.menu.calc_menu, menu);
                return true;
            case 1:
                menu.clear();
                getMenuInflater().inflate(R.menu.conv_menu, menu);
                return true;
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.show_graph)
            startActivity(new Intent(this, GraphActivity.class));
        else switch (tabLayout.getSelectedTabPosition()) {
            case 0:
                ComputationEngine calc = (ComputationEngine) ((ViewPagerAdapter) viewPager.getAdapter()).getItem(0);
                calc.handleOptionsItemSelected(getSupportFragmentManager(), item.getItemId());
                break;
            case 1:
                ConversionEngine conv = (ConversionEngine) ((ViewPagerAdapter) viewPager.getAdapter()).getItem(1);
                conv.handleOptionsItemSelected(getSupportFragmentManager(), item.getItemId());
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new ComputationEngine(), ("Calculator").toUpperCase(Locale.getDefault()));
        adapter.addFrag(new ConversionEngine(), ("Unit Converter").toUpperCase(Locale.getDefault()));
        viewPager.setAdapter(adapter);
    }

    public void reportListDialogResult(int callerId, int which) {
        switch (callerId) {
            case 0:
                ((ComputationEngine) ((ViewPagerAdapter) viewPager.getAdapter()).getItem(0)).reportListDialogResult(which);
                break;
        }
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
