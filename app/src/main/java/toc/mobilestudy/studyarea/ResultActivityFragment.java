package toc.mobilestudy.studyarea;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import toc.mobilestudy.R;

/**
 * Created by user on 2/6/2016.
 */
public class ResultActivityFragment extends Fragment{
    public static ProgressBar progressBar;

        public ResultActivityFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_result, container, false);
            Intent i = getActivity().getIntent();
            String uri = i.getStringExtra("FILETOOPEN");
            progressBar = (ProgressBar) rootView.findViewById(R.id.webLoader);
            progressBar.setVisibility(View.VISIBLE);
            WebView webView;
            webView = (WebView) rootView.findViewById(R.id.webView);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.loadUrl(uri);
            webView.setWebViewClient(new DisPlayWebPageActivityClient());
            getActivity().setTitle(i.getStringExtra("title"));
            //progressBar.setVisibility(View.GONE);
            return rootView;
        }

    static class DisPlayWebPageActivityClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onLoadResource(WebView view, String url) {
            super.onLoadResource(view, url);
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            progressBar.setVisibility(View.GONE);
        }
    }
}

