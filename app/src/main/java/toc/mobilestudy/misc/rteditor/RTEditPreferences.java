package toc.mobilestudy.misc.rteditor;

import android.os.Bundle;
import android.preference.PreferenceActivity;

import toc.mobilestudy.R;


/* EditPreferences
 * 		Simple activity that just displays the preferences
 * 		nothing really different here */
public class RTEditPreferences extends PreferenceActivity
{  	
	public void onCreate(Bundle savedInstanceState)
	{  
		super.onCreate(savedInstanceState);  

		// add preferences
		addPreferencesFromResource(R.xml.pref_rte);
	}
}
