package toc.mobilestudy.misc;

/**
 * Created by user on 6/8/2016.
 */

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import toc.mobilestudy.R;

/**
 * Created by user on 2/24/2016.
 */
public class AppItemAdapter extends BaseAdapter {


    private final List<AppItem> items;
    private final Context context;

    public AppItemAdapter(Context context, List<AppItem> items) {
        this.items = items;
        this.context = context;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = View.inflate(context, R.layout.browse_item, null);
            holder = new ViewHolder();
            holder.itemTitle = (TextView) convertView.findViewById(R.id.name_view);
            holder.itemIcon = (ImageView) convertView.findViewById(R.id.image_view);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.itemTitle.setText(items.get(position).getAppName());
        holder.itemIcon.setImageDrawable(items.get(position).getAppIcon());

        return convertView;
    }

    static class ViewHolder {
        TextView itemTitle;
        ImageView itemIcon;
        public View layout;
    }


}
