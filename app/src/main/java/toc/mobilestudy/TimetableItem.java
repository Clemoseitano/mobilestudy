package toc.mobilestudy;

import android.database.Cursor;

/**
 * Created by Clement Osei Tano K on 18/05/2017.
 * The content here is free software unless
 * it contains parts that can be claimed to
 * be proprietary, in which case you the user
 * is required to update this header to contain
 * the appropriate license header.
 * This software is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

public class TimetableItem {
    private int day;
    private long startTime;
    private long endTime;
    private int periods;
    private String title;
    private long itemId;
    private String location;
    private String lecturer;

    TimetableItem() {

    }

    TimetableItem(String title, int day, long start, long end, int periods, long itemId) {
        this.title = title;
        this.day = day;
        this.startTime = start;
        this.endTime = end;
        this.periods = periods;
        this.itemId = itemId;
    }

    public TimetableItem(Cursor c) {
        this.title = c.getString(c.getColumnIndex(TimetableDbHelper.ITEM_NAME));
        this.day = c.getInt(c.getColumnIndex(TimetableDbHelper.ITEM_DAY));
        this.startTime = c.getLong(c.getColumnIndex(TimetableDbHelper.START_TIME));
        this.endTime = c.getLong(c.getColumnIndex(TimetableDbHelper.END_TIME));
        this.periods = c.getInt(c.getColumnIndex(TimetableDbHelper.PERIOD_COUNT));
        this.itemId = c.getLong(c.getColumnIndex(TimetableDbHelper.ID));
        this.lecturer = c.getString(c.getColumnIndex(TimetableDbHelper.LECTURER));
        this.location = c.getString(c.getColumnIndex(TimetableDbHelper.LOCATION));
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public int getPeriods() {
        return periods;
    }

    public void setPeriods(int periods) {
        this.periods = periods;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getItemId() {
        return itemId;
    }

    public void setItemId(long itemId) {
        this.itemId = itemId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLecturer() {
        return lecturer;
    }

    public void setLecturer(String lecturer) {
        this.lecturer = lecturer;
    }
}
