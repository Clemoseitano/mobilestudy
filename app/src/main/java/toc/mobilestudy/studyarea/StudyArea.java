package toc.mobilestudy.studyarea;


import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.artifex.mupdfdemo.MuPDFActivity;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import toc.mobilestudy.AppContentProvider;
import toc.mobilestudy.Constants;
import toc.mobilestudy.DateHelper;
import toc.mobilestudy.FavDbHelper;
import toc.mobilestudy.R;
import toc.mobilestudy.fileexplorer.FileAdapter;
import toc.mobilestudy.fileexplorer.FileDialog;
import toc.mobilestudy.misc.rteditor.RTEditorActivity;
import toc.mobilestudy.note.VideoPlayer;
import toc.mobilestudy.note.activity.AudioPlayer;
import toc.mobilestudy.studyarea.epub.MainActivity;

public class StudyArea extends AppCompatActivity implements AbsListView.OnItemClickListener, FileDialog.FileListener {
    private ArrayList<NoteItem> ITEMS = new ArrayList<>();

    private AbsListView listView;
    private int favs = 0;
    private SharedPreferences prefs;
    private SharedPreferences.Editor ed;


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        fillData();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.item_list);
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        updateStatus();

        listView = (AbsListView) findViewById(R.id.list);
        listView.setOnItemClickListener(this);
        findViewById(R.id.placeholder).setVisibility(View.GONE);
        FloatingActionButton fabAddSchedule = (FloatingActionButton) findViewById(R.id.add_schedule);
        fabAddSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createNote();
            }
        });
        registerForContextMenu(listView);
    }

    private void updateStatus() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDefaultDisplayHomeAsUpEnabled(true);
            Window window = getWindow();
            if (Build.VERSION.SDK_INT >= 21) {
                actionBar.setElevation(0);
            }

            actionBar.setDisplayOptions(ActionBar.DISPLAY_HOME_AS_UP | ActionBar.DISPLAY_SHOW_TITLE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.setStatusBarColor(getResources().getColor(R.color.primary));
            }
            actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimaryDark)));

        }
    }

    @Override
    protected void onResume() {
        fillData();
        super.onResume();
    }

    private void fillData() {
        Cursor favsCursor = getContentResolver().query(AppContentProvider.FAV_URI, null, null, null,
                prefs.getString(Constants.SORT_ORDER, StudyAreaConstants.KEY_NAME));
        Cursor remindersCursor = getContentResolver().query(AppContentProvider.BOOK_URI, null, null, null,
                prefs.getString(Constants.SORT_ORDER, StudyAreaConstants.KEY_NAME));
        //check if its empty
        if (remindersCursor != null || favsCursor != null) {
            if ((remindersCursor != null && remindersCursor.getCount() <= 0) && (favsCursor != null && favsCursor.getCount() <= 0)) {
                findViewById(R.id.container).setVisibility(View.INVISIBLE);
                findViewById(R.id.placeholder).setVisibility(View.VISIBLE);
                new LoadTask().execute();
                return;
            } else {
                findViewById(R.id.container).setVisibility(View.VISIBLE);
                findViewById(R.id.placeholder).setVisibility(View.GONE);
            }
            if (favsCursor != null)
                favs = favsCursor.getCount();
            ITEMS.clear();
            if (favsCursor != null && favsCursor.getCount() > 0) {
                if (Build.VERSION.SDK_INT >= 21)
                    ITEMS.add(new NoteItem("separator", "Favourites", ""));
                favsCursor.moveToFirst();

                do {
                    NoteItem newItem = new NoteItem(favsCursor);
                    ITEMS.add(newItem);
                } while (favsCursor.moveToNext());
            }

            if (remindersCursor != null && remindersCursor.getCount() > 0) {
                if (Build.VERSION.SDK_INT >= 21)
                    ITEMS.add(new NoteItem("separator", "Local Files", ""));
                remindersCursor.moveToFirst();
                do {
                    NoteItem newItem = new NoteItem(remindersCursor);
                    ITEMS.add(newItem);
                } while (remindersCursor.moveToNext());
            }
            final NoteItemAdapter reminders = new NoteItemAdapter(StudyArea.this, ITEMS);
            ((AdapterView<ListAdapter>) listView).setAdapter(reminders);
            listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View arg1,
                                               final int position, long arg3) {
                    final NoteItem item = (NoteItem) parent.getAdapter().getItem(position);
                    CharSequence[] items;
                    items = new CharSequence[2];
                    items[0] = "View Details";
                    items[1] = "Delete Item";


                    AlertDialog.Builder builder = new AlertDialog.Builder(StudyArea.this);
                    builder.setTitle("Library Item");
                    builder.setItems(items, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case 1:
                                    if (position <= favs)
                                        getContentResolver().delete(AppContentProvider.FAV_URI, StudyAreaConstants.KEY_ID + "=" + item.getItemId(), null);
                                    else
                                        getContentResolver().delete(AppContentProvider.BOOK_URI, StudyAreaConstants.KEY_ID + "=" + item.getItemId(), null);
                                    reminders.notifyDataSetChanged();
                                    fillData();
                                    break;
                                case 0:
                                    showDetailsDialog(item);
                                    break;
                            }
                        }
                    });
                    builder.create().show();
                    return true;
                }
            });
            if (favsCursor != null)
                favsCursor.close();
            if (remindersCursor != null)
                remindersCursor.close();
        }
    }

    private void showDetailsDialog(NoteItem item) {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.content_item_details, (ViewGroup) findViewById(R.id.link_layout));
        final TextView noteName = (TextView) layout.findViewById(R.id.name);
        final TextView noteLink = (TextView) layout.findViewById(R.id.address);
        final TextView noteType = (TextView) layout.findViewById(R.id.type);
        final TextView noteSize = (TextView) layout.findViewById(R.id.size);
        final TextView noteDate = (TextView) layout.findViewById(R.id.date);

        noteDate.setText(item.getDateModified());
        noteName.setText(item.getName());
        noteLink.setText(item.getLink());
        noteType.setText(item.getType());
        File f = new File(item.getLink());
        if (f.exists() && !f.isDirectory())
            noteSize.setText(FileAdapter.formatFileSize(f));
        else if (f.exists() && f.isDirectory()) noteSize.setText(R.string.folder);
        else noteSize.setText("--");
        new AlertDialog.Builder(StudyArea.this)
                .setTitle(R.string.item_details)
                .setView(layout)
                .setPositiveButton(R.string.okay, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater mi = getMenuInflater();
        mi.inflate(R.menu.menu_study_area, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_sort:
                sortItemsDialog();
                return true;
            case R.id.menu_settings:
                new LoadTask().execute();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void sortItemsDialog() {
        ed = prefs.edit();
        CharSequence[] items;
        items = new CharSequence[3];
        items[0] = "Name";
        items[1] = "Date";
        items[2] = "Type";
        //items[3] = "Directory";use as update


        AlertDialog.Builder builder = new AlertDialog.Builder(StudyArea.this);
        builder.setTitle("Sort by...");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        ed.putString(Constants.SORT_ORDER, StudyAreaConstants.KEY_NAME);
                        ed.apply();
                        fillData();
                        break;
                    case 1:
                        ed.putString(Constants.SORT_ORDER, StudyAreaConstants.KEY_DATE_MODIFIED);
                        ed.apply();
                        fillData();
                        break;
                    case 2:
                        ed.putString(Constants.SORT_ORDER, StudyAreaConstants.KEY_TYPE);
                        ed.apply();
                        fillData();
                        break;
                }
            }
        });
        builder.create().show();
    }

    private void createNote() {
        SharedPreferences prefs = getPreferences(MODE_PRIVATE);
        FileDialog f = new FileDialog();
        Bundle arguments = new Bundle();
        arguments.putInt(FileDialog.ARG_REQUEST_CODE, 3456);
        f.setMode(FileDialog.Mode.PICK_FILE);
        f.setTitle("Pick File");
        f.setTempDirectory(prefs.getString(Constants.LAST_FILE,
                Environment.getExternalStorageDirectory().getAbsolutePath()));
        f.setArguments(arguments);
        f.show(getSupportFragmentManager(), "FileDialog");
    }


    @Override
    public void onFileReturned(int requestcode, File file) {
        String type = "Unknown";
        if (file.getName().toLowerCase().endsWith(".pdf"))
            type = "pdf";
        else if ((file.getName().toLowerCase().endsWith(".epub")))
            type = "epub";
        else if ((file.getName().toLowerCase().endsWith(".html") || file.getName().toLowerCase().
                endsWith(".xhtml") || file.getName().toLowerCase().endsWith(".xml") ||
                file.getName().toLowerCase().endsWith(".htm")))
            type = "web";
        else if (file.getName().toLowerCase().contains("."))
            type = file.getName().toLowerCase().substring(file.getName().toLowerCase().lastIndexOf("."));
        ContentValues values = new ContentValues();
        values.put(StudyAreaConstants.KEY_NAME, file.getName());
        values.put(StudyAreaConstants.KEY_TYPE, type);
        values.put(StudyAreaConstants.KEY_LINK, file.getAbsolutePath());
        values.put(StudyAreaConstants.KEY_DATE_TAKEN, DateHelper.convertDateToString(new Date(System.currentTimeMillis())));
        values.put(StudyAreaConstants.KEY_DATE_MODIFIED, DateHelper.convertDateToString(new Date(file.lastModified())));
        getContentResolver().insert(AppContentProvider.FAV_URI, values);
        Toast.makeText(StudyArea.this, "File: " + file.getName() + " added to Library",
                Toast.LENGTH_LONG).show();
        SharedPreferences prefs = getPreferences(MODE_PRIVATE);
        prefs.edit().putString(Constants.LAST_FILE, file.getParent()).apply();
        fillData();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        fillData();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {//TODO: make
        // this use filetype instead
        NoteItem item = (NoteItem) parent.getAdapter().getItem(position);
        String name = item.getName();
        if (name.toLowerCase().endsWith(".pdf")) {
            Intent intent = new Intent(StudyArea.this, MuPDFActivity.class);
            intent.setData(Uri.parse(item.getLink()));
            startActivity(intent);
        } else if (name.toLowerCase().endsWith(".epub")) {
            Intent intent = new Intent(StudyArea.this, MainActivity.class);
            intent.setData(Uri.parse(item.getLink()));
            startActivity(intent);
        } else if (name.toLowerCase().endsWith(".html") || name.toLowerCase().
                endsWith(".xhtml") || name.toLowerCase().endsWith(".xml") ||
                item.getName().toLowerCase().endsWith(".htm"))
            startActivity(new Intent(StudyArea.this, ResultActivity.class).
                    putExtra("FILETOOPEN", "file://" + item.getLink()));
        else fileOptionDialog(item);
    }

    private void fileOptionDialog(final NoteItem item) {
        CharSequence[] items;
        items = new CharSequence[7];
        items[0] = "Text";
        items[1] = "Video";
        items[2] = "Image";
        items[3] = "Audio";
        items[4] = "PDF File";
        items[5] = "EPUB File";
        items[6] = "HTML File";


        AlertDialog.Builder builder = new AlertDialog.Builder(StudyArea.this);
        builder.setTitle("Library Item");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent;
                switch (which) {
                    case 0:
                        intent = new Intent(StudyArea.this, RTEditorActivity.class);
                        intent.setData(Uri.parse(item.getLink()));
                        startActivity(intent);
                        break;
                    case 1:
                        intent = new Intent(StudyArea.this, VideoPlayer.class);
                        intent.setData(Uri.parse(item.getLink()));
                        startActivity(intent);
                        break;
                    case 2:
                        intent = new Intent(StudyArea.this, toc.mobilestudy.imageviewer.MainActivity.class);
                        intent.setData(Uri.parse(item.getLink()));
                        startActivity(intent);
                        break;
                    case 3:
                        intent = new Intent(StudyArea.this, AudioPlayer.class);
                        intent.putExtra("uri", item.getLink());
                        intent.putExtra("name", item.getName());
                        startActivity(intent);
                        break;
                    case 4:
                        intent = new Intent(StudyArea.this, MuPDFActivity.class);
                        intent.setData(Uri.parse(item.getLink()));
                        startActivity(intent);
                        break;
                    case 5:
                        intent = new Intent(StudyArea.this, MainActivity.class);
                        intent.setData(Uri.parse(item.getLink()));
                        startActivity(intent);
                        break;
                    case 6:
                        startActivity(new Intent(StudyArea.this, ResultActivity.class).
                                putExtra("FILETOOPEN", "file://" + item.getLink()));
                        break;

                }
            }
        });
        builder.create().show();
    }

    public class LoadTask extends AsyncTask<String, Integer, String> {
        public LoadTask() {

        }

        @Override
        protected String doInBackground(String... args) {

            if (Constants.mounted || new File(Constants.SD_CARD).exists()) {
                getContentResolver().delete(AppContentProvider.BOOK_URI, StudyAreaConstants.KEY_ID + ">" + 0, null);
                File sdcard = new File(Constants.SD_CARD);
                String[] extensions = new String[]{"pdf", "epub", "html", "xhtml", "htm"};
                Collection<File> files = (Collection<File>) FileUtils.listFiles(sdcard, extensions, true);
                for (File file : files) {
                    String type = "Unknown";
                    if (file.getName().toLowerCase().endsWith(".pdf"))
                        type = "pdf";
                    else if ((file.getName().toLowerCase().endsWith(".epub")))
                        type = "epub";
                    else if ((file.getName().toLowerCase().endsWith(".html") || file.getName().toLowerCase().
                            endsWith(".xhtml")|| file.getName().toLowerCase().endsWith(".htm")))
                        type = "web";
                    if (!isFav(AppContentProvider.md5(file.getAbsolutePath()))) {
                        ContentValues values = new ContentValues();
                        values.put(StudyAreaConstants.KEY_NAME, file.getName());
                        values.put(StudyAreaConstants.KEY_TYPE, type);
                        values.put(StudyAreaConstants.KEY_LINK, file.getAbsolutePath());
                        values.put(StudyAreaConstants.KEY_DATE_TAKEN, DateHelper.convertDateToString(new Date(System.currentTimeMillis())));
                        values.put(StudyAreaConstants.KEY_DATE_MODIFIED, DateHelper.convertDateToString(new Date(file.lastModified())));
                        getContentResolver().insert(AppContentProvider.BOOK_URI, values);
                    }
                }

            }

            return null;
        }

        @Override
        public void onPostExecute(final String username) {
            fillData();
        }
    }

    public boolean isFav(String hash) {
        Cursor mCursor = getContentResolver().query(AppContentProvider.FAV_URI, new String[]{StudyAreaConstants.KEY_ID},
                FavDbHelper.KEY_HASH + "='" + hash + "'", null, null);
        boolean b = false;
        if (mCursor != null) {
            b = mCursor.getCount() > 0;
            mCursor.close();
        }
        return b;
    }
}
