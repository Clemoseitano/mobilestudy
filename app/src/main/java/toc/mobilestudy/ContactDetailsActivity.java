package toc.mobilestudy;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.soundcloud.android.crop.Crop;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import static toc.mobilestudy.fileexplorer.FileAdapter.getFileExtension;

public class ContactDetailsActivity extends AppCompatActivity {
    public static final String URL = "full_link";
    public static final String NAME = "display_name";
    public static final String USER = "user_name";
    private static final int PERMISSION = 879;
    private ImageView profilePicture;
    String imageUrl = "none";
    String uname;
    private boolean resolving = false;
    SharedPreferences prefs;

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_contact_details);
        String title = getResources().getString(R.string.app_name);
        if (getIntent().hasExtra(URL))
            imageUrl = getIntent().getStringExtra(URL);
        if (getIntent().hasExtra(NAME))
            title = getIntent().getStringExtra(NAME);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        if (actionBar != null) {
            actionBar.setDefaultDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_TITLE | ActionBar.DISPLAY_HOME_AS_UP);
            Window window = getWindow();
            if (Build.VERSION.SDK_INT >= 21) {
                actionBar.setElevation(0);
                window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
            }
            actionBar.setDefaultDisplayHomeAsUpEnabled(true);
            actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));
            actionBar.setTitle(title);
        }

        TextView username = (TextView) findViewById(R.id.username);
        profilePicture = (ImageView) findViewById(R.id.profile_pic);
        if (getIntent().hasExtra(USER))
            uname = getIntent().getStringExtra(USER);
        if (uname == null)
            uname = title;
        username.setText(uname);
        TextDrawable drw = createTextDrawable(title);
        Glide.with(this).load(imageUrl).centerCrop().placeholder(drw).error(drw).into(profilePicture);
        profilePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (resolving)
                    resolving = false;
                checkStoragePermission();
                if (!resolving) {
                        Crop.pickImage(ContactDetailsActivity.this);
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    public TextDrawable createTextDrawable(String feedName) {
        String lettersForName = getResources().getString(R.string.app_name);
        ColorGenerator generator = ColorGenerator.DEFAULT;
        int color = generator.getRandomColor();
        if (feedName == null)
            return TextDrawable.builder().buildRect(lettersForName, color);

        String[] names = feedName.split(" ");
        lettersForName = "";
        for (String n : names)
            lettersForName = lettersForName + n.substring(0, 1);
        lettersForName = lettersForName.toUpperCase();
        return TextDrawable.builder().buildRect(lettersForName, color);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent result) {
        if (requestCode == Crop.REQUEST_PICK && resultCode == RESULT_OK) {
            beginCrop(result.getData());
        } else if (requestCode == Crop.REQUEST_CROP) {
            handleCrop(resultCode, result);
        }
    }

    private void beginCrop(Uri source) {
        File g = new File(Constants.FILES_PATH);
        boolean b = g.mkdirs();
        if (!g.exists() && !b)
            Toast.makeText(this, getResources().getString(R.string.error_creating_file), Toast.LENGTH_SHORT).show();
        Uri destination = Uri.fromFile(new File(Constants.FILES_PATH, "cropped"));
        Crop.of(source, destination).asSquare().start(this);
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            TextDrawable drw = createTextDrawable(getDisplayName());
            Glide.with(this).load(Crop.getOutput(result)).centerCrop().placeholder(drw).error(drw).into(profilePicture);
            String fn = Crop.getOutput(result).toString();
            if (fn != null && fn.contains(":"))
                fn = fn.split(":")[1];
            else return;
            File fi = new File(fn);
            Bitmap selectedBitmap = BitmapFactory.decodeFile(fi.getAbsolutePath());
            if (selectedBitmap != null) {
                FileOutputStream out = null;
                try {
                    new File(Constants.PROFILE_PIC).mkdirs();
                    File f = new File(Constants.PROFILE_PIC, Constants.PIC);
                    if (f.exists())
                        f.delete();
                    out = new FileOutputStream(new File(Constants.PROFILE_PIC, Constants.PIC));
                    selectedBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
                    uploadFile(new File(Constants.PROFILE_PIC, Constants.PIC).getAbsolutePath());
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (out != null) {
                            out.flush();
                            out.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private String getDisplayName() {
        return prefs.getString(Constants.PREF_KEY_NAME, null);
    }

    private void uploadFile(final String filePath) {
        //checking if file is available
        if (filePath != null) {
            //displaying progress dialog while image is uploading
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading");
            progressDialog.show();
            final DatabaseReference mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
            StorageReference storageReference = FirebaseStorage.getInstance().getReference();
            StorageReference sRef = storageReference.child(Constants.STORAGE_PATH_UPLOADS).
                    child(getUser()).child("profile_pic" + "." + getFileExtension(filePath));

            //adding the file to reference
            sRef.putFile(Uri.fromFile(new File(filePath))).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @SuppressWarnings("VisibleForTests")
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    progressDialog.dismiss();
                    String downUrl = taskSnapshot.getUploadSessionUri().toString();
                    Toast.makeText(getApplicationContext(), "File Uploaded ", Toast.LENGTH_LONG).show();
                    mFirebaseDatabaseReference.child("users").child(getUser()).child("profile_picture").setValue(downUrl);
                    mFirebaseDatabaseReference.child("users").child(getUser()).child("profile_hash").setValue(md5(new File(filePath)));
                    SharedPreferences.Editor ed = prefs.edit();
                    ed.putString(Constants.DP, downUrl);
                    ed.apply();
                }
            })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @SuppressWarnings("VisibleForTests")
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            //displaying the upload progress
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                            progressDialog.setMessage("Uploaded " + ((int) progress) + "%...");
                        }
                    });
        } else {
            Toast.makeText(this, getResources().getString(R.string.file_not_found), Toast.LENGTH_LONG).show();
            //display an error if no file is selected
        }
    }

    private String getUser() {
        return prefs.getString(Constants.PREF_KEY_USER, null);
    }

    public static String md5(File s) {
        MessageDigest digest;
        try {
            InputStream in = new FileInputStream(s);
            byte[] buf = new byte[(int) s.length()];
            in.read(buf);
            ByteBuffer buffer = ByteBuffer.wrap(buf);
            digest = MessageDigest.getInstance("MD5");
            digest.update(buffer);
            byte[] magnitude = digest.digest();
            BigInteger bi = new BigInteger(1, magnitude);
            String hash = String.format("%0" + (magnitude.length << 1) + "x", bi);
            return hash;
        } catch (NoSuchAlgorithmException ignored) {

        } catch (IOException e) {
            e.printStackTrace();
        }
        return "xyzzy";
    }

    private void checkStoragePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int hasStoragePermission = checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE);
            List<String> permissions = new ArrayList<>();
            if (hasStoragePermission != PackageManager.PERMISSION_GRANTED)
                permissions.add(android.Manifest.permission.READ_EXTERNAL_STORAGE);
            if (!permissions.isEmpty() && !resolving) {
                requestPermissions(permissions.toArray(new String[permissions.size()]), PERMISSION);
                resolving = true;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION:
                for (int i = 0; i < permissions.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        Snackbar.make(profilePicture, "Jelo has not been allowed access to view files.", Snackbar.LENGTH_LONG).setAction("Change", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                openSetting();
                                resolving = false;
                            }
                        }).show();
                        resolving = true;
                    } else checkStoragePermission();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void openSetting() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + getPackageName()));
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
