package toc.mobilestudy.studyarea.epub;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;

import java.io.File;

import toc.mobilestudy.R;
import toc.mobilestudy.fileexplorer.FileDialog;
import toc.mobilestudy.studyarea.epub.WebServer.FileRequestHandler;
import toc.mobilestudy.studyarea.epub.WebServer.ServerSocketThread;
import toc.mobilestudy.studyarea.epub.WebServer.WebServer;
import toc.mobilestudy.studyarea.epub.epub.Book;
import toc.mobilestudy.studyarea.epub.epub.TableOfContents;

public class MainActivity extends AppCompatActivity implements IResourceSource, FileDialog.FileListener {
    private final static int LIST_EPUB_ACTIVITY_ID = 0;
    private final static int LIST_CHAPTER_ACTIVITY_ID = 1;
    private final static int CHECK_TTS_ACTIVITY_ID = 2;

    /*
     * the app's main view
     */
    private EpubWebView mEpubWebView;

    TextToSpeechWrapper mTtsWrapper;

    private ServerSocketThread mWebServerThread = null;

    private void updateStatus() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) { actionBar.setDefaultDisplayHomeAsUpEnabled(true);
            Window window = getWindow();
            if (Build.VERSION.SDK_INT >= 21) {
                actionBar.setElevation(0);
            }

            actionBar.setDisplayOptions(ActionBar.DISPLAY_HOME_AS_UP | ActionBar.DISPLAY_SHOW_TITLE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.setStatusBarColor(getResources().getColor(R.color.primary));
            }
            actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimaryDark)));

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_epub);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        updateStatus();
        mEpubWebView = createView(findViewById(R.id.book_view));
        mTtsWrapper = new TextToSpeechWrapper();
        mWebServerThread = createWebServer();
        mWebServerThread.startThread();

        Intent intent = getIntent();
        newIntent(intent);
        if (savedInstanceState != null) {
            // screen orientation changed, reload
            mEpubWebView.gotoBookmark(new Bookmark(savedInstanceState));
        } else {
            // app has just been started.
            // If a bookmark has been saved, go to it, else, ask user for epub
            // to view
            Bookmark bookmark = new Bookmark(this);
            if (bookmark.isEmpty()) {
                launchBookList();
            } else {
                mEpubWebView.gotoBookmark(bookmark);
            }
        }
    }


    private ServerSocketThread createWebServer() {
        FileRequestHandler handler = new FileRequestHandler(this);
        WebServer server = new WebServer(handler);
        return new ServerSocketThread(server, Globals.WEB_SERVER_PORT);
    }

    private EpubWebView createView(View viewById) {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1) {
            return (EpubWebView23) viewById;
        } else {
            return (EpubWebView30) viewById;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.menu_pick_epub:
                createNote();
                return true;
            case R.id.menu_set_bookmark:
                Bookmark bookmark = mEpubWebView.getBookmark();
                if (bookmark != null) {
                    bookmark.saveToSharedPreferences(MainActivity.this);
                }
                return true;
            case R.id.menu_goto_bookmark:
                mEpubWebView.gotoBookmark(new Bookmark(MainActivity.this));
                return true;
            case R.id.menu_start_speech:
                mTtsWrapper.checkTextToSpeech(MainActivity.this, CHECK_TTS_ACTIVITY_ID);
                mEpubWebView.speak(mTtsWrapper);
                return true;
            case R.id.menu_stop_speech:
                mTtsWrapper.stop();
                return true;
            case R.id.menu_chapters:
                launchChaptersList();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void launchBookList() {
        Intent listComicsIntent = new Intent(this, ListEpubActivity.class);
        startActivityForResult(listComicsIntent, LIST_EPUB_ACTIVITY_ID);
    }

    private void launchChaptersList() {
        Book book = getBook();
        if (book == null) {
            Utility.showToast(this, R.string.no_book_selected);
        } else {
            TableOfContents toc = book.getTableOfContents();
            if (toc.size() == 0) {
                Utility.showToast(this, R.string.table_of_contents_missing);
            } else {
                Intent listChaptersIntent = new Intent(this, ListChaptersActivity.class);
                toc.pack(listChaptersIntent, ListChaptersActivity.CHAPTERS_EXTRA);
                startActivityForResult(listChaptersIntent, LIST_CHAPTER_ACTIVITY_ID);
            }
        }
    }

    /*
     * Should return with epub or chapter to load
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CHECK_TTS_ACTIVITY_ID) {
            mTtsWrapper.checkTestToSpeechResult(this, resultCode);
            return;
        }
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case LIST_EPUB_ACTIVITY_ID:
                    onListEpubResult(data);
                    break;

                case LIST_CHAPTER_ACTIVITY_ID:
                    onListChapterResult(data);
                    break;

                default:
                    Utility.showToast(this, R.string.something_is_badly_broken);
            }
        } else if (resultCode == RESULT_CANCELED) {
            Utility.showErrorToast(this, data);
        }
    }

    private void onListEpubResult(Intent data) {
        String fileName = data.getStringExtra(ListEpubActivity.FILENAME_EXTRA);
        loadEpub(fileName, null);
    }

    private void onListChapterResult(Intent data) {
        Uri chapterUri = data.getParcelableExtra(ListChaptersActivity.CHAPTER_EXTRA);
        mEpubWebView.loadChapter(chapterUri);
    }

    private void loadEpub(String fileName, Uri chapterUri) {
        mEpubWebView.setBook(fileName);
        mEpubWebView.loadChapter(chapterUri);
        ActionBar ab=getSupportActionBar();
        if(ab!=null)
            ab.setSubtitle(fileName.split("/")[fileName.split("/").length-1]);
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Bookmark bookmark = mEpubWebView.getBookmark();
        if (bookmark != null) {
            bookmark.save(outState);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mTtsWrapper.onDestroy();
        mWebServerThread.stopThread();
    }

    /*
     * Book currently being used.
     * (Hack to provide book to WebServer.)
     */
    public Book getBook() {
        return mEpubWebView.getBook();
    }

    public void newIntent(Intent intent) {
        setIntent(intent);
        Uri mUri;
        mUri = intent.getData();

        if (mUri != null) {

            loadEpub(mUri.getPath(), null);
        }

    }

    private void createNote() {
        FileDialog f = new FileDialog();
        Bundle arguments = new Bundle();
        arguments.putInt(FileDialog.ARG_REQUEST_CODE, 3456);
        f.setMode(FileDialog.Mode.PICK_FILE);
        f.setTitle("Pick File");
        f.setTempDirectory(Environment.getExternalStorageDirectory().getAbsolutePath());
        f.setArguments(arguments);
        f.show(getSupportFragmentManager(), "FileDialog");
    }

    @Override
    public void onFileReturned(int requestcode, File file) {
        Intent intent = new Intent();
        intent.setData(Uri.parse(file.getAbsolutePath()));
        newIntent(intent);
    }
    @Override
    public ResourceResponse fetch(Uri resourceUri) {
        return getBook().fetch(resourceUri);
    }
}
