package toc.mobilestudy.misc;

import android.content.Context;
import android.database.Cursor;
import android.graphics.drawable.Drawable;

import toc.mobilestudy.R;
import toc.mobilestudy.misc.tutorials.TutorialActivity;
import toc.mobilestudy.misc.tutorials.TutorialDbAdapter;

/**
 * Created by user on 6/8/2016.
 */
public class AppItem {
    private String appName;
    private Class appActivity;
    private Drawable appIcon;
    private String type;
    public String bundleExtraSiteLink;
    public String bundleExtraSiteCompiler;


    public AppItem(String appName, Class appActivity, Drawable appIcon) {
        this.appName = appName;
        this.appActivity = appActivity;
        this.appIcon = appIcon;
        this.type = "app";
    }

    public AppItem(Cursor c, Context context) {
        this.type = "site";
        this.appName = c.getString(c.getColumnIndexOrThrow(TutorialDbAdapter.KEY_NAME));
                this.bundleExtraSiteLink= c.getString(c.getColumnIndexOrThrow(TutorialDbAdapter.KEY_SITE_LINK));
        this.bundleExtraSiteCompiler=c.getString(c.getColumnIndexOrThrow(TutorialDbAdapter.KEY_COMPILER));
        if(appName.contains("C++"))
        this.appIcon = context.getResources().getDrawable(R.drawable.cpp);
        else if(appName.contains("Python"))
            this.appIcon = context.getResources().getDrawable(R.drawable.ic_py);
        else
            this.appIcon = context.getResources().getDrawable(R.drawable.ic_unknown);
        this.appActivity = TutorialActivity.class;
    }

    public Class getAppActivity() {
        return appActivity;
    }

    public void setAppActivity(Class appActivity) {
        this.appActivity = appActivity;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public Drawable getAppIcon() {
        return appIcon;
    }

    public void setAppIcon(Drawable appIcon) {
        this.appIcon = appIcon;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
