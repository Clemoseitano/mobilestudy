package toc.mobilestudy.studyarea;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.widget.Toast;

import toc.mobilestudy.AppContentProvider;
import toc.mobilestudy.BookDbHelper;
import toc.mobilestudy.DateHelper;
import toc.mobilestudy.FavDbHelper;

import java.util.Date;

/**
 * Created by user on 4/4/2016.
 */
public class NoteItem {
    private String type;
    private String dateTaken;
    private String link;
    private String dateModified;
    private String name;
    private Long itemId;

    public NoteItem(String type, String name, String link) {
        this.type = type;
        this.link = link;
        this.name = name;
        this.dateTaken = DateHelper.convertDateToString(new Date(System.currentTimeMillis()));
        this.dateModified = DateHelper.convertDateToString(new Date(System.currentTimeMillis()));
        this.itemId = -1L;
    }

    public NoteItem(String type, String name, String link, String dateTaken) {
        this.type = type;
        this.link = link;
        this.name = name;
        this.dateTaken = dateTaken;
        this.dateModified = DateHelper.convertDateToString(new Date(System.currentTimeMillis()));
        this.itemId = -1L;
    }

    public NoteItem(Cursor data) {
        this.type = data.getString(data.getColumnIndex(StudyAreaConstants.KEY_TYPE));
        this.name = data.getString(data.getColumnIndex(StudyAreaConstants.KEY_NAME));
        this.link = data.getString(data.getColumnIndex(StudyAreaConstants.KEY_LINK));
        this.itemId = data.getLong(data.getColumnIndex(StudyAreaConstants.KEY_ID));
        this.dateTaken = data.getString(data.getColumnIndex(StudyAreaConstants.KEY_DATE_TAKEN));
        this.dateModified = data.getString(data.getColumnIndex(StudyAreaConstants.KEY_DATE_MODIFIED));
    }

    public NoteItem(Context context, String name) {
        String[] args = {name};
        String sel = StudyAreaConstants.KEY_NAME + " = ? ";
        String[] projection = new String[]{StudyAreaConstants.KEY_TYPE, StudyAreaConstants.KEY_NAME,
                StudyAreaConstants.KEY_DATE_TAKEN, StudyAreaConstants.KEY_LINK};
        Cursor cursor;
        cursor = context.getContentResolver().query(AppContentProvider.BOOK_URI, null,
                StudyAreaConstants.KEY_NAME + "='" + name + "'", null, null);
        if (null == cursor) {
            Toast.makeText(context, "An error occurred while finding match for file with name " + "\' " +
                    name + " \'", Toast.LENGTH_LONG).show();
        } else if (cursor.getCount() < 1) {
            Toast.makeText(context, "No match for file with name " + "\' " +
                    name + " \'", Toast.LENGTH_LONG).show();
            cursor.close();
        } else {
            try {
                new NoteItem(cursor, name);
            } finally {
                cursor.close();
            }

        }

    }

    public NoteItem(Cursor data, String name) {
        data.moveToFirst();
        this.type = data.getString(data.getColumnIndex(StudyAreaConstants.KEY_TYPE));
        this.name = name;
        this.link = data.getString(data.getColumnIndex(StudyAreaConstants.KEY_LINK));
        this.itemId = data.getLong(data.getColumnIndex(StudyAreaConstants.KEY_ID));
        this.dateTaken = data.getString(data.getColumnIndex(StudyAreaConstants.KEY_DATE_TAKEN));
        this.dateModified = DateHelper.convertDateToString(new Date(System.currentTimeMillis()));
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDateTaken() {
        return dateTaken;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDateModified() {
        return dateModified;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ContentValues getContentValues() {
        ContentValues c = new ContentValues();
        c.put(StudyAreaConstants.KEY_TYPE, this.type);
        c.put(StudyAreaConstants.KEY_DATE_MODIFIED, this.dateModified);
        c.put(StudyAreaConstants.KEY_DATE_TAKEN, this.dateTaken);
        c.put(StudyAreaConstants.KEY_NAME, this.name);
        c.put(StudyAreaConstants.KEY_LINK, this.link);
        return c;
    }

    public long getItemId() {
        return this.itemId;
    }
}
