package toc.mobilestudy;

/**
 * Created by Clement Osei Tano K on 18/05/2017.
 * The content here is free software unless
 * it contains parts that can be claimed to
 * be proprietary, in which case you the user
 * is required to update this header to contain
 * the appropriate license header.
 * This software is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import toc.mobilestudy.note.activity.NoteHome;
import toc.mobilestudy.note.fragment.AudioItemFragment;
import toc.mobilestudy.note.fragment.ImageItemFragment;
import toc.mobilestudy.note.fragment.NoteItemFragment;
import toc.mobilestudy.note.fragment.TextItemFragment;
import toc.mobilestudy.note.fragment.VideoItemFragment;
import toc.mobilestudy.timelineview.TimelineView;

public class TimelineActivity extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private List<TimetableItem> mDataList = new ArrayList<>();
    private TimelineView.Orientation mOrientation;
    private boolean mWithLinePadding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timeline);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        mOrientation = prefs.getBoolean(Constants.VERTICAL_TIMELINE, true) ? TimelineView.Orientation.VERTICAL : TimelineView.Orientation.HORIZONTAL;
        mWithLinePadding = false;
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mRecyclerView.setLayoutManager(getLinearLayoutManager());
        mRecyclerView.setHasFixedSize(true);
        initView();
    }

    private void initView() {
        setDataListItems();
        final TimeLineAdapter adapter = new TimeLineAdapter(mDataList, mOrientation, mWithLinePadding);
        adapter.setOnItemClickListener(new TimeLineAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                TimetableItem i = adapter.getItem(position);
                if (i.getItemId() == -1L)
                    return;
                Intent intent = new Intent(TimelineActivity.this, AddTimetableActivity.class);
                intent.putExtra(AddTimetableActivity.ITEM_ROW, (int) (i.getItemId()));
                startActivity(intent);
            }
        });
        mRecyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater mi = getMenuInflater();
        mi.inflate(R.menu.menu_timeline, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //Menu
        switch (item.getItemId()) {
            //When home is clicked
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_add_timetable:
                startActivity(new Intent(this, AddTimetableActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus)
            initView();
    }

    private LinearLayoutManager getLinearLayoutManager() {
        if (mOrientation == TimelineView.Orientation.HORIZONTAL) {
            return new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        } else {
            return new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        }
    }

    private void setDataListItems() {
        mDataList.clear();
        Calendar cal = new GregorianCalendar();
        cal.setTimeInMillis(System.currentTimeMillis());
        int i = cal.get(Calendar.DAY_OF_WEEK);
        int cycle = 0;
        String[] arr = getResources().getStringArray(R.array.day_display_values);
        while (cycle < 7) {
            String dayname = arr[(i - 1) % 7];
            mDataList.add(new TimetableItem(dayname, (i - 1) % 7, 0L, 60 * 60 * 1000 * 24, 1, -1L));
            String selection = TimetableDbHelper.ITEM_DAY + " = " + (i - 1) % 7;
            Cursor c = getContentResolver().query(AppContentProvider.TT_URI, null, selection, null, TimetableDbHelper.START_TIME + " ASC");
            if (c != null && c.getCount() > 0) {
                c.moveToFirst();
                do {
                    mDataList.add(new TimetableItem(c));
                } while (c.moveToNext());
                c.close();
            }
            i = i + 1;
            cycle = cycle + 1;
        }
    }
}
