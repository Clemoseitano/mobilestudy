package toc.mobilestudy.misc.drawpad;

/**
 * Created by user on 5/30/2016.
 */

import android.graphics.Paint;
import android.graphics.Path;

public class PathWithPaint {
    private Path path;

    public Path getPath() {
        return path;
    }

    public void setPath(Path path) {
        this.path = path;
    }

    private Paint mPaint;

    public Paint getmPaint() {
        return mPaint;
    }

    public void setmPaint(Paint mPaint) {
        this.mPaint = mPaint;
    }
}