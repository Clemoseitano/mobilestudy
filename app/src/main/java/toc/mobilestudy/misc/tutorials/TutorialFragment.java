package toc.mobilestudy.misc.tutorials;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import toc.mobilestudy.R;


public class TutorialFragment extends Fragment {
    public static final String ARG_ITEM_TITLE = "item_title";
    public static final String ARG_ITEM_LINK = "item_link";
    private static ProgressBar progressBar;
    SharedPreferences prefs;

    public TutorialFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden)
            updateTitle();
    }

    private void updateTitle() {
        if (getArguments().containsKey(ARG_ITEM_TITLE)) {

            Activity activity = this.getActivity();
            Toolbar appBarLayout = (Toolbar) activity.findViewById(R.id.toolbar);
            if (appBarLayout != null) {
                appBarLayout.setTitle(getArguments().getString(ARG_ITEM_TITLE));
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.newsitem_detail, container, false);

        if (getArguments().getString(ARG_ITEM_LINK) != null) {
            WebView webView;
            progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
            webView = (WebView) rootView.findViewById(R.id.newsitem_detail);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setBuiltInZoomControls(true);

            if (Build.VERSION.SDK_INT < 11)
                webView.getSettings().setPluginState(WebSettings.PluginState.ON);
            webView.setWebViewClient(new ArticleWebViewClient());
            webView.setWebChromeClient(new WebChromeClient());
            webView.getSettings().setAppCacheMaxSize(5 * 1024 * 1024); // 5MiB
            webView.getSettings().setAppCachePath(getContext().getCacheDir().getAbsolutePath());
            webView.getSettings().setAllowFileAccess(true);
            webView.getSettings().setAppCacheEnabled(true);
            webView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                webView.getSettings().setDisplayZoomControls(false);
            }

            if (!isNetworkAvailable() || prefs.getBoolean("cached_webpage", false)) { // loading offline
                webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
            } else
                webView.loadUrl(getArguments().getString(ARG_ITEM_LINK));
            webView.setWebViewClient(new ArticleWebViewClient());
        }

        return rootView;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    class ArticleWebViewClient extends WebViewClient {

        private ArticleWebViewClient() {

        }

        public void onPageFinished(WebView webView, String url) {
            super.onPageFinished(webView, url);
            if (isNetworkAvailable())
                prefs.edit().putBoolean("cached_webpage", true).apply();
            progressBar.setVisibility(View.GONE);
        }

        public void onPageStarted(WebView webView, String url, Bitmap favicon) {
            progressBar.setVisibility(View.VISIBLE);
            super.onPageStarted(webView, url, favicon);
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String url) {
            webView.loadUrl(url);
            return true;
        }
    }
}
