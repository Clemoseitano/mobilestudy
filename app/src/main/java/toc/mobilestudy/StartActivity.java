package toc.mobilestudy;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.bumptech.glide.Glide;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import toc.mobilestudy.misc.AppItem;
import toc.mobilestudy.misc.AppItemAdapter;
import toc.mobilestudy.misc.calculator.CalculatorActivity;
import toc.mobilestudy.misc.drawpad.MainActivity;
import toc.mobilestudy.misc.rteditor.RTEditorActivity;
import toc.mobilestudy.misc.tutorials.TutorialActivity;
import toc.mobilestudy.misc.tutorials.TutorialDbAdapter;
import toc.mobilestudy.note.activity.NoteHome;
import toc.mobilestudy.scheduler.ScheduleActivity;
import toc.mobilestudy.studyarea.StudyArea;

public class StartActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, AdapterView.OnItemClickListener {

    private static final int SELECT_PHOTO = 2643;
    private static final int RESULT_CROP = 5843;
    private ImageView imageView;
    private DrawerLayout drawer;
    SharedPreferences prefs;
    private ArrayList<AppItem> ITEMS = new ArrayList<>();
    private AbsListView listView;
    private TutorialDbAdapter mDbHelper;
    private TextView nameView;


    private void updateStatus() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDefaultDisplayHomeAsUpEnabled(true);
            Window window = getWindow();
            if (Build.VERSION.SDK_INT >= 21) {
                actionBar.setElevation(0);
            }

            actionBar.setDisplayOptions(ActionBar.DISPLAY_HOME_AS_UP | ActionBar.DISPLAY_SHOW_TITLE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.setStatusBarColor(getResources().getColor(toc.mobilestudy.R.color.primary));
            }
            actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(toc.mobilestudy.R.color.colorPrimaryDark)));

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefs = getPreferences(MODE_PRIVATE);
        prefs = PreferenceManager.getDefaultSharedPreferences(this);

        setContentView(R.layout.starter);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        updateStatus();

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View navHeader = navigationView.getHeaderView(0);
        imageView = ((ImageView) navHeader.findViewById(R.id.imageView));

        nameView = ((TextView) navHeader.findViewById(R.id.user_name));

        listView = (AbsListView) findViewById(R.id.list);

        listView.setOnItemClickListener(this);

        FloatingActionButton add = (FloatingActionButton) findViewById(R.id.add_schedule);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createTutorial();
            }
        });

    }

    private void loadUserData() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    final String name = prefs.getString(Constants.PREF_KEY_NAME, null);
                    final String photo = prefs.getString(Constants.DP, null);
                    final String usname = prefs.getString(Constants.PREF_KEY_USER, null);
                    final TextDrawable drw = createTextDrawable(name);
                    StartActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            nameView.setText(name);
                            if (photo != null)
                                Glide.with(StartActivity.this).load(photo).placeholder(drw).error(drw).into(imageView);
                            else imageView.setImageDrawable(drw);
                            imageView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(StartActivity.this, ContactDetailsActivity.class);
                                    intent.putExtra(ContactDetailsActivity.URL, photo);
                                    intent.putExtra(ContactDetailsActivity.NAME, name);
                                    intent.putExtra(ContactDetailsActivity.USER, usname);
                                    startActivity(intent);
                                }
                            });
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public TextDrawable createTextDrawable(String feedName) {
        String lettersForName = getResources().getString(R.string.app_name);
        ColorGenerator generator = ColorGenerator.DEFAULT;
        int color = generator.getRandomColor();
        if (feedName == null)
            return TextDrawable.builder().buildRect(lettersForName, color);

        String[] names = feedName.split(" ");
        lettersForName = "";
        for (String n : names)
            lettersForName = lettersForName + n.substring(0, 1);
        lettersForName = lettersForName.toUpperCase();
        return TextDrawable.builder().buildRect(lettersForName, color);
    }
    @Override
    protected void onResume() {
        loadUserData();
        mDbHelper = new TutorialDbAdapter(this).open();
        fillData();
        super.onResume();
    }


    private void fillData() {
        findViewById(toc.mobilestudy.R.id.placeholder).setVisibility(View.GONE);
        ITEMS.clear();

        ITEMS.add(new AppItem("Calculator", CalculatorActivity.class, getResources().getDrawable(R.drawable.ic_calc)));

        ITEMS.add(new AppItem("DrawPad", MainActivity.class, getResources().getDrawable(R.drawable.paint)));

        ITEMS.add(new AppItem("Note Editor", RTEditorActivity.class, getResources().getDrawable(R.drawable.ic_not)));

        ITEMS.add(new AppItem("Library", StudyArea.class, getResources().getDrawable(R.drawable.images)));


        Cursor performanceCursor = mDbHelper.fetchAllEntries();
        //check if its empty
        if (performanceCursor.getCount() > 0) {
            performanceCursor.moveToFirst();
            do {
                AppItem newItem = new AppItem(performanceCursor, this);
                ITEMS.add(newItem);
            } while (performanceCursor.moveToNext());
        }
        AppItemAdapter reminders = new AppItemAdapter(this, ITEMS);
        ((AdapterView<ListAdapter>) listView).setAdapter(reminders);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus)
            fillData();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {

            case SELECT_PHOTO:

                if (resultCode == Activity.RESULT_OK && data != null && data.getData() != null) {
                    try {
                        Uri selectedImage = data.getData();
                        String[] filePathColumn = {MediaStore.Images.Media.DATA};
                        Cursor cursor = getContentResolver().query(selectedImage,
                                filePathColumn, null, null, null);
                        if (cursor != null) {
                            cursor.moveToFirst();
                            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                            String picturePath = cursor.getString(columnIndex);
                            cursor.close();

                            performCrop(picturePath);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;

            case RESULT_CROP:
                if (resultCode == Activity.RESULT_OK) {
                    Bundle extras = data.getExtras();
                    Bitmap selectedBitmap = extras.getParcelable("data");
                    if (selectedBitmap != null) {
                        FileOutputStream out = null;
                        try {
                            new File(Constants.PROFILE_PIC).mkdirs();
                            File f = new File(Constants.PROFILE_PIC, Constants.PIC);
                            if (f.exists())
                                f.delete();
                            out = new FileOutputStream(new File(Constants.PROFILE_PIC, Constants.PIC));
                            selectedBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            try {
                                if (out != null) {
                                    out.flush();
                                    out.close();
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    imageView.setImageBitmap(selectedBitmap);
                    imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                }
                break;
        }
    }

    private void performCrop(String picUri) {
        try {
            //Start Crop Activity

            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            // indicate image type and Uri
            File f = new File(picUri);
            Uri contentUri = Uri.fromFile(f);

            cropIntent.setDataAndType(contentUri, "image/*");
            // set crop properties
            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 320);
            cropIntent.putExtra("outputY", 320);

            // retrieve data on return
            cropIntent.putExtra("return-data", true);
            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, RESULT_CROP);
        }
        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {
            // display an error message
            String errorMessage = "your device doesn't support the crop action!";
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.personal_notes) {
            startActivity(new Intent(StartActivity.this, NoteHome.class));
        } else if (id == R.id.schedules) {
            startActivity(new Intent(StartActivity.this, ScheduleActivity.class));
        } else if (id == R.id.timetable) {
            startActivity(new Intent(this, TimelineActivity.class));
        }


        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        AppItem item = (AppItem) parent.getAdapter().getItem(position);
        if (item.getType().equals("app"))
            startActivity(new Intent(StartActivity.this, item.getAppActivity()));
        else {
            Intent in = new Intent(StartActivity.this, TutorialActivity.class);
            in.putExtra("name", item.getAppName());
            in.putExtra("site", item.bundleExtraSiteLink);
            in.putExtra("compiler", item.bundleExtraSiteCompiler);
            startActivity(in);
        }
    }

    public void createTutorial() {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.content_start, (ViewGroup) findViewById(R.id.link_layout));
        final EditText noteName = (EditText) layout.findViewById(R.id.name_text);
        final EditText noteLink = (EditText) layout.findViewById(R.id.url_text);
        final EditText noteDes = (EditText) layout.findViewById(R.id.acc_url_text);

        new AlertDialog.Builder(StartActivity.this)
                .setTitle("Add QuickLink")
                .setView(layout)
                .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        if ((noteName.getText() == null && noteLink.getText() == null) || (noteName.getText() == null && noteDes.getText() == null))
                            return;
                        String name = noteName.getText().toString();
                        String link = noteLink.getText().toString();
                        String acc = noteDes.getText().toString();
                        if (TextUtils.isEmpty(name) || (TextUtils.isEmpty(link)))
                            return;
                        if (link.contains("http://") || link.contains("https://"))
                            link = link.trim();
                        else link = "http://" + link.trim();

                        if (acc.contains("http://") || acc.contains("https://"))
                            acc = acc.trim();
                        else acc = "http://" + acc.trim();
                        new TutorialDbAdapter(StartActivity.this).open().createEntry(name, link, acc);
                        fillData();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                })
                .show();
    }
}
